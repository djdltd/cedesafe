/*++

Copyright (c) 1999-2002  Microsoft Corporation

Module Name:

    scanuk.h

Abstract:

    Header file which contains the structures, type definitions,
    constants, global variables and function prototypes that are
    shared between kernel and user mode.

Environment:

    Kernel & user mode

--*/

#ifndef __SCANUK_H__
#define __SCANUK_H__

//
//  Name of port used to communicate
//

const PWSTR ScannerPortName = L"\\SwapBufferComms";


#define SCANNER_READ_BUFFER_SIZE   1024
#define VOLUMENAMEBUFFERSIZE	256

typedef struct _SCANNER_NOTIFICATION {

    ULONG BytesToScan;
    ULONG Reserved;             // for quad-word alignement of the Contents structure
	int NotifyType;			// Is this a notification for read or write
	ULONG processID;
	PVOID origBuf;
	ULONG origBufSize;
    UCHAR Contents[SCANNER_READ_BUFFER_SIZE];
    UCHAR Volname[VOLUMENAMEBUFFERSIZE];

} SCANNER_NOTIFICATION, *PSCANNER_NOTIFICATION;

typedef struct _SCANNER_REPLY {

    BOOLEAN SafeToOpen;
    
} SCANNER_REPLY, *PSCANNER_REPLY;

#endif //  __SCANUK_H__


