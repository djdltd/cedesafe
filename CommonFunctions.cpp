#include "CommonFunctions.h"

CommonFunctions::CommonFunctions ()
{

}

CommonFunctions::~CommonFunctions ()
{

}

bool CommonFunctions::FileExists (char *szFilepath)
{
	struct _finddata_t c_file;
	long hFile;

	if( (hFile = _findfirst(szFilepath, &c_file )) == -1L ) {
		return false;
	}
	else
	{
		return true;
	}
}

bool CommonFunctions::LocalFileExists (char *szFilename)
{
	// Load the file into the memory buffer
	char szModule[SIZE_STRING];
	ZeroMemory (szModule, SIZE_STRING);
	GetModuleFileName (NULL, szModule, SIZE_STRING);

	char szPath[SIZE_STRING];
	ZeroMemory (szPath, SIZE_STRING);

	GetPathOnly (szModule, szPath);

	strcat_s (szPath, SIZE_STRING, szFilename);

	if (FileExists (szPath) == true) {
		return true;
	} else {
		return false;
	}
}

void CommonFunctions::GetPathOnly (char *szInpath, char *szOutpath)
{
	char szCurchar[SIZE_NAME];
	int seploc = 0;

	for (int c=strlen(szInpath);c>0;c--) {
		
		ZeroMemory (szCurchar, SIZE_NAME);
		strncpy_s (szCurchar, SIZE_NAME, szInpath+c, 1);

		if (strcmp (szCurchar, "\\") == 0) {
			seploc = c+1;
			break;
		}
	}

	strncpy_s (szOutpath, SIZE_STRING, szInpath, seploc);
}

void CommonFunctions::SIDLToFile (DynList *pdl, char *szFilename)
{
	// Simple function. Take a DynList full of StringItems and serialise
	// it, then save it to a file.
	// NOTE: szFilename is a FILENAME ONLY NOT A FULL PATH
	//		 the file will be saved in the same location as the app.
	// SIDL = String Item Dyn List
	
	MemoryBuffer memSerialised;
	StringItem *pitem;

	int inumentries = pdl->GetNumItems ();

	if (inumentries == 0) {
		return;
	}

	int i = 0;

	int totalsize = sizeof (int);
	int currentsize = 0;
	char szCurrentitem[SIZE_STRING];
	
	// First we need to work out the size of the serialised buffer
	for (i=0;i<inumentries;i++) {
		pitem = (StringItem *) pdl->GetItem (i);
		
		totalsize+=sizeof (int); // to account for the size of the item
		totalsize+=strlen (pitem->szItem); // the size of the string itself
	}

	// Now set the size of the serialised buffer
	memSerialised.SetSize (totalsize);

	// Now append the number of items to the buffer
	memSerialised.Append (&inumentries, sizeof (int));

	// Now go through each item and append it to the buffer
	for (i=0;i<inumentries;i++) {
		pitem = (StringItem *) pdl->GetItem (i);

		ZeroMemory (szCurrentitem, SIZE_STRING);
		currentsize = strlen (pitem->szItem);
		strcpy_s (szCurrentitem, SIZE_STRING, pitem->szItem);

		memSerialised.Append (&currentsize, sizeof (int)); // Append the item size
		memSerialised.Append ((char *) szCurrentitem, currentsize); // Append the item itself

	}

	// Now save the serialised buffer to disk
	char szModule[SIZE_STRING];
	ZeroMemory (szModule, SIZE_STRING);
	GetModuleFileName (NULL, szModule, SIZE_STRING);

	char szPath[SIZE_STRING];
	ZeroMemory (szPath, SIZE_STRING);

	GetPathOnly (szModule, szPath);

	strcat_s (szPath, SIZE_STRING, szFilename);

	memSerialised.SaveToFile (szPath);


	// Now clean up
	memSerialised.Clear ();
}

void CommonFunctions::FileToSIDL (DynList *pdl, char *szFilename) 
{
	// Reverse of the function above. Take a file and convert it to a
	// DynList of StringItems.

	StringItem curItem;
	int icursize = 0;
	MemoryBuffer memSerialised;
	int inumitems = 0;
	int ipointer = 0;
	int i = 0;

	// Load the file into the memory buffer
	char szModule[SIZE_STRING];
	ZeroMemory (szModule, SIZE_STRING);
	GetModuleFileName (NULL, szModule, SIZE_STRING);

	char szPath[SIZE_STRING];
	ZeroMemory (szPath, SIZE_STRING);

	GetPathOnly (szModule, szPath);

	strcat_s (szPath, SIZE_STRING, szFilename);

	if (memSerialised.ReadFromFile (szPath) == true) {
		pdl->Clear ();

		memcpy (&inumitems, (BYTE *) memSerialised.GetBuffer ()+ipointer, sizeof (int));
		ipointer+=sizeof (int);

		for (i=0;i<inumitems;i++) {
			
			// The size of the string item
			memcpy (&icursize, (BYTE *) memSerialised.GetBuffer ()+ipointer, sizeof (int));
			ipointer += sizeof (int);

			// The item data or string
			ZeroMemory (curItem.szItem, SIZE_STRING);
			if (icursize > 0) {
				strncpy_s (curItem.szItem, SIZE_STRING, (char *) memSerialised.GetBuffer ()+ipointer, icursize);
			}
			ipointer+=icursize;

			// Now add this item to the dynlist			
			pdl->AddItem (&curItem, sizeof (StringItem), false);
		}

	}

	memSerialised.Clear ();
}

void CommonFunctions::SIDLToListBox (HWND hwndlistbox, DynList *pdl)
{
	StringItem *pitem;
	int i = 0;

	// First clear the listbox
	SendMessage (hwndlistbox, LB_RESETCONTENT, 0, 0);

	for (i=0;i<pdl->GetNumItems ();i++) {
		pitem = (StringItem *) pdl->GetItem (i);

		// Add the string item to the list box
		SendMessage (hwndlistbox, LB_ADDSTRING, 0, (LPARAM) pitem->szItem);
	}
}

void CommonFunctions::ListBoxToSIDL (HWND hwndlistbox, DynList *pdl)
{
	StringItem curItem;
	int i = 0;
	pdl->Clear (); // Clear the destination dynlist

	int inumlbitems = SendMessage (hwndlistbox, LB_GETCOUNT, 0, 0);

	for (i=0;i<inumlbitems;i++) {
		
		ZeroMemory (curItem.szItem, SIZE_STRING);
		SendMessage (hwndlistbox, LB_GETTEXT, i, (LPARAM) curItem.szItem);
		
		pdl->AddItem (&curItem, sizeof (StringItem), false);
	}
}

bool CommonFunctions::DoesListBoxStringExist (HWND hwndlistbox, char *szItem)
{
	char szLocalitem[SIZE_STRING];
	ZeroMemory (szLocalitem, SIZE_STRING);

	strcpy_s (szLocalitem, SIZE_STRING, szItem);
	_strupr (szLocalitem);

	char szCurrent[SIZE_STRING];
	
	int i = 0;

	int inumlbitems = SendMessage (hwndlistbox, LB_GETCOUNT, 0, 0);

	for (i=0;i<inumlbitems;i++) {
		ZeroMemory (szCurrent, SIZE_STRING);
		SendMessage (hwndlistbox, LB_GETTEXT, i, (LPARAM) szCurrent);

		_strupr (szCurrent);

		if (strcmp (szCurrent, szLocalitem) == 0) {
			return true;
		}
	}

	return false;
}

bool CommonFunctions::DoesSIExist (DynList *pdl, char *szItem)
{
	int i = 0;
	StringItem *pitem;

	char szLocal[SIZE_STRING];
	ZeroMemory (szLocal, SIZE_STRING);
	strcpy_s (szLocal, SIZE_STRING, szItem);
	_strupr (szLocal);

	char szCurrent[SIZE_STRING];
	
	for (i=0;i<pdl->GetNumItems ();i++) {
		pitem = (StringItem *) pdl->GetItem (i);

		ZeroMemory (szCurrent, SIZE_STRING);
		strcpy_s (szCurrent, SIZE_STRING, pitem->szItem);
		_strupr (szCurrent);

		if (strcmp (szCurrent, szLocal) == 0) {
			return true;
		}
	}

	return false;
}
