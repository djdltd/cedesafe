#include "UserSettings.h"

UserSettings::UserSettings ()
{
	bUseIncludeFolders = false;
	bAutoStart = true;
	bUseAlerts = true;
}

UserSettings::~UserSettings ()
{

}

void UserSettings::Load ()
{
	if (m_reg.DoesDWSettingExist ("UseIncludeFolders") == true) {
		DWORD dwValue = m_reg.ReadDWORDSetting ("UseIncludeFolders");

		if (dwValue == 1) {
			bUseIncludeFolders = true;
		} else {
			bUseIncludeFolders = false;
		}
	} else {
		bUseIncludeFolders = false;	
	}

	if (m_reg.DoesDWSettingExist ("AutoStart") == true) {
		DWORD dwValue = m_reg.ReadDWORDSetting ("AutoStart");

		if (dwValue == 1) {
			bAutoStart = true;
		} else {
			bAutoStart = false;
		}
	} else {
		bAutoStart = true;	
	}

	if (m_reg.DoesDWSettingExist ("UseAlerts") == true) {
		DWORD dwValue = m_reg.ReadDWORDSetting ("UseAlerts");

		if (dwValue == 1) {
			bUseAlerts = true;
		} else {
			bUseAlerts = false;
		}
	} else {
		bUseAlerts = true;	
	}

}

void UserSettings::Save ()
{
	if (bUseIncludeFolders == true) {
		m_reg.WriteDWORDSetting ("UseIncludeFolders", 1);
	} else {
		m_reg.WriteDWORDSetting ("UseIncludeFolders", 0);
	}

	if (bAutoStart == true) {
		m_reg.WriteDWORDSetting ("AutoStart", 1);
	} else {
		m_reg.WriteDWORDSetting ("AutoStart", 0);
	}


	if (bUseAlerts == true) {
		m_reg.WriteDWORDSetting ("UseAlerts", 1);
	} else {
		m_reg.WriteDWORDSetting ("UseAlerts", 0);
	}
}