// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "BaseWindowTemplate.h"

BaseWindowTemplate::BaseWindowTemplate ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
	m_bInitialised = false;
}

BaseWindowTemplate::~BaseWindowTemplate ()
{

}

void BaseWindowTemplate::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (GetSysColor (COLOR_BTNFACE));
	SetCaption (TEXT ("Application Window"));
	SetWindowStyle (FS_STYLESTANDARD);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "APPWindowClass");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	if (m_bInitialised == false) {
		m_bInitialised = true;
		CreateAppWindow (m_szClassname, 70, 0, 427, 185, true);
	}
	
	SetWindowPosition (FS_CENTER);
	SetAlwaysOnTop (true);
	Show ();
}

void BaseWindowTemplate::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void BaseWindowTemplate::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;	
	g_hWnd = hWnd;
	
	
	HFONT hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);

}

void BaseWindowTemplate::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
	switch (LOWORD (wParam)) {
		
	}
}


void BaseWindowTemplate::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{

}

void BaseWindowTemplate::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{

	}
}

void BaseWindowTemplate::OnTimer (WPARAM wParam)
{	

}

void BaseWindowTemplate::OnPaint (HWND hWnd)
{

}

void BaseWindowTemplate::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	

}

void BaseWindowTemplate::OnLButtonDown (HWND hWnd)
{

}

void BaseWindowTemplate::OnLButtonUp (HWND hWnd)
{

}