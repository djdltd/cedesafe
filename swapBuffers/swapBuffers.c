/*++

Copyright (c) 1999 - 2002  Microsoft Corporation

Module Name:

    SwapBuffers.c

Abstract:

    This is a sample filter which demonstrates proper access of data buffer
    and a general guideline of how to swap buffers.
    For now it only swaps buffers for:

    IRP_MJ_READ
    IRP_MJ_WRITE
    IRP_MJ_DIRECTORY_CONTROL

    By default this filter attaches to all volumes it is notified about.  It
    does support having multiple instances on a given volume.

Environment:

    Kernel mode

--*/

#include <fltKernel.h>
#include <dontuse.h>
#include <suppress.h>

#pragma prefast(disable:__WARNING_ENCODE_MEMBER_FUNCTION_POINTER, "Not valid for kernel mode drivers")


PFLT_FILTER gFilterHandle;

/*************************************************************************
    Pool Tags
*************************************************************************/

#define BUFFER_SWAP_TAG     'bdBS'
#define CONTEXT_TAG         'xcBS'
#define NAME_TAG            'mnBS'
#define PRE_2_POST_TAG      'ppBS'

/*************************************************************************
    Local structures
*************************************************************************/

const PWSTR CommsPortName = L"\\SwapBufferComms";
PFLT_PORT ServerPort;
PEPROCESS UserProcess;
int UserProcessID;
PFLT_PORT ClientPort;

#define SWAP_READ_BUFFER_SIZE   1024
#define VOLUMENAMEBUFFERSIZE	256

typedef struct _SWAP_NOTIFICATION {

    ULONG BytesToScan;
    ULONG Reserved;             // for quad-word alignement of the Contents structure
	int NotifyType;			// Is this a notification for read or write
	ULONG processID;
	PVOID origBuf;
	ULONG origBufSize;
    UCHAR Contents[SWAP_READ_BUFFER_SIZE];
	UCHAR Volname[VOLUMENAMEBUFFERSIZE];

} SWAP_NOTIFICATION, *PSWAP_NOTIFICATION;

typedef struct _SWAP_REPLY {

    BOOLEAN IsEncrypted;
    
} SWAP_REPLY, *PSWAP_REPLY;

//
//  This is a volume context, one of these are attached to each volume
//  we monitor.  This is used to get a "DOS" name for debug display.
//

const UNICODE_STRING ScannerExtensionsToScan[] =
    { RTL_CONSTANT_STRING( L"doc"),
      RTL_CONSTANT_STRING( L"txt"),
      RTL_CONSTANT_STRING( L"bat"),
      RTL_CONSTANT_STRING( L"cmd"),
      RTL_CONSTANT_STRING( L"inf"),
      /*RTL_CONSTANT_STRING( L"ini"),   Removed, to much usage*/
      {0, 0, NULL}
    };

typedef struct _VOLUME_CONTEXT {

    //
    //  Holds the name to display
    //

    UNICODE_STRING Name;

    //
    //  Holds the sector size for this volume.
    //

    ULONG SectorSize;

} VOLUME_CONTEXT, *PVOLUME_CONTEXT;

#define MIN_SECTOR_SIZE 0x200


//
//  This is a context structure that is used to pass state from our
//  pre-operation callback to our post-operation callback.
//

typedef struct _PRE_2_POST_CONTEXT {

    //
    //  Pointer to our volume context structure.  We always get the context
    //  in the preOperation path because you can not safely get it at DPC
    //  level.  We then release it in the postOperation path.  It is safe
    //  to release contexts at DPC level.
    //

    PVOLUME_CONTEXT VolCtx;

    //
    //  Since the post-operation parameters always receive the "original"
    //  parameters passed to the operation, we need to pass our new destination
    //  buffer to our post operation routine so we can free it.
    //

    PVOID SwappedBuffer;

} PRE_2_POST_CONTEXT, *PPRE_2_POST_CONTEXT;

//
//  This is a lookAside list used to allocate our pre-2-post structure.
//

NPAGED_LOOKASIDE_LIST Pre2PostContextList;

/*************************************************************************
    Prototypes
*************************************************************************/

NTSTATUS
InstanceSetup (
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __in FLT_INSTANCE_SETUP_FLAGS Flags,
    __in DEVICE_TYPE VolumeDeviceType,
    __in FLT_FILESYSTEM_TYPE VolumeFilesystemType
    );

VOID
CleanupVolumeContext(
    __in PFLT_CONTEXT Context,
    __in FLT_CONTEXT_TYPE ContextType
    );

NTSTATUS
InstanceQueryTeardown (
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __in FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags
    );

NTSTATUS
CommsPortConnect (
    __in PFLT_PORT lClientPort,
    __in_opt PVOID ServerPortCookie,
    __in_bcount_opt(SizeOfContext) PVOID ConnectionContext,
    __in ULONG SizeOfContext,
    __deref_out_opt PVOID *ConnectionCookie
    );

VOID
CommsPortDisconnect (
     __in_opt PVOID ConnectionCookie
     );

NTSTATUS
DriverEntry (
    __in PDRIVER_OBJECT DriverObject,
    __in PUNICODE_STRING RegistryPath
    );

NTSTATUS
FilterUnload (
    __in FLT_FILTER_UNLOAD_FLAGS Flags
    );

FLT_PREOP_CALLBACK_STATUS
SwapPreCreate (
    __inout PFLT_CALLBACK_DATA Data,
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __deref_out_opt PVOID *CompletionContext
    );

FLT_POSTOP_CALLBACK_STATUS
SwapPostCreate (
    __inout PFLT_CALLBACK_DATA Data,
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __in_opt PVOID CompletionContext,
    __in FLT_POST_OPERATION_FLAGS Flags
    );

FLT_PREOP_CALLBACK_STATUS
SwapPreClose (
    __inout PFLT_CALLBACK_DATA Data,
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __deref_out_opt PVOID *CompletionContext
    );

FLT_POSTOP_CALLBACK_STATUS
SwapPostClose (
    __inout PFLT_CALLBACK_DATA Data,
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __deref_out_opt PVOID *CompletionContext,
	__in FLT_POST_OPERATION_FLAGS Flags
    );

VOID
ReadDriverParameters (
    __in PUNICODE_STRING RegistryPath
    );

//
//  Assign text sections for each routine.
//

#ifdef ALLOC_PRAGMA
#pragma alloc_text(PAGE, InstanceSetup)
#pragma alloc_text(PAGE, CleanupVolumeContext)
#pragma alloc_text(PAGE, InstanceQueryTeardown)
#pragma alloc_text(INIT, DriverEntry)
#pragma alloc_text(INIT, ReadDriverParameters)
#pragma alloc_text(PAGE, FilterUnload)
#endif

//
//  Operation we currently care about.
//

CONST FLT_OPERATION_REGISTRATION Callbacks[] = {

	{ IRP_MJ_CREATE,
      0,
      SwapPreCreate,
      SwapPostCreate},
	
	{ IRP_MJ_CLOSE,
		0,
		SwapPreClose,
		SwapPostClose},

    { IRP_MJ_OPERATION_END }
};

//
//  Context definitions we currently care about.  Note that the system will
//  create a lookAside list for the volume context because an explicit size
//  of the context is specified.
//

CONST FLT_CONTEXT_REGISTRATION ContextNotifications[] = {

     { FLT_VOLUME_CONTEXT,
       0,
       CleanupVolumeContext,
       sizeof(VOLUME_CONTEXT),
       CONTEXT_TAG },

     { FLT_CONTEXT_END }
};

//
//  This defines what we want to filter with FltMgr
//

CONST FLT_REGISTRATION FilterRegistration = {

    sizeof( FLT_REGISTRATION ),         //  Size
    FLT_REGISTRATION_VERSION,           //  Version
    0,                                  //  Flags

    ContextNotifications,               //  Context
    Callbacks,                          //  Operation callbacks

    FilterUnload,                       //  MiniFilterUnload

    InstanceSetup,                      //  InstanceSetup
    InstanceQueryTeardown,              //  InstanceQueryTeardown
    NULL,                               //  InstanceTeardownStart
    NULL,                               //  InstanceTeardownComplete

    NULL,                               //  GenerateFileName
    NULL,                               //  GenerateDestinationFileName
    NULL                                //  NormalizeNameComponent

};

/*************************************************************************
    Debug tracing information
*************************************************************************/

//
//  Definitions to display log messages.  The registry DWORD entry:
//  "hklm\system\CurrentControlSet\Services\Swapbuffers\DebugFlags" defines
//  the default state of these logging flags
//

#define LOGFL_ERRORS    0x00000001  // if set, display error messages
#define LOGFL_READ      0x00000002  // if set, display READ operation info
#define LOGFL_WRITE     0x00000004  // if set, display WRITE operation info
#define LOGFL_DIRCTRL   0x00000008  // if set, display DIRCTRL operation info
#define LOGFL_VOLCTX    0x00000010  // if set, display VOLCTX operation info

ULONG LoggingFlags = 0;             // all disabled by default

#define LOG_PRINT( _logFlag, _string )                              \
    (FlagOn(LoggingFlags,(_logFlag)) ?                              \
        DbgPrint _string  :                                         \
        ((void)0))

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
//
//                      Routines
//
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


NTSTATUS
InstanceSetup (
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __in FLT_INSTANCE_SETUP_FLAGS Flags,
    __in DEVICE_TYPE VolumeDeviceType,
    __in FLT_FILESYSTEM_TYPE VolumeFilesystemType
    )
/*++

Routine Description:

    This routine is called whenever a new instance is created on a volume.

    By default we want to attach to all volumes.  This routine will try and
    get a "DOS" name for the given volume.  If it can't, it will try and
    get the "NT" name for the volume (which is what happens on network
    volumes).  If a name is retrieved a volume context will be created with
    that name.

Arguments:

    FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
        opaque handles to this filter, instance and its associated volume.

    Flags - Flags describing the reason for this attach request.

Return Value:

    STATUS_SUCCESS - attach
    STATUS_FLT_DO_NOT_ATTACH - do not attach

--*/
{
    PDEVICE_OBJECT devObj = NULL;
    PVOLUME_CONTEXT ctx = NULL;
    NTSTATUS status;
    ULONG retLen;
    PUNICODE_STRING workingName;
    USHORT size;
    UCHAR volPropBuffer[sizeof(FLT_VOLUME_PROPERTIES)+512];
    PFLT_VOLUME_PROPERTIES volProp = (PFLT_VOLUME_PROPERTIES)volPropBuffer;

    PAGED_CODE();

    UNREFERENCED_PARAMETER( Flags );
    UNREFERENCED_PARAMETER( VolumeDeviceType );
    UNREFERENCED_PARAMETER( VolumeFilesystemType );

    try {

        //
        //  Allocate a volume context structure.
        //

        status = FltAllocateContext( FltObjects->Filter,
                                     FLT_VOLUME_CONTEXT,
                                     sizeof(VOLUME_CONTEXT),
                                     NonPagedPool,
                                     &ctx );

        if (!NT_SUCCESS(status)) {

            //
            //  We could not allocate a context, quit now
            //

            leave;
        }

        //
        //  Always get the volume properties, so I can get a sector size
        //

        status = FltGetVolumeProperties( FltObjects->Volume,
                                         volProp,
                                         sizeof(volPropBuffer),
                                         &retLen );

        if (!NT_SUCCESS(status)) {

            leave;
        }

        //
        //  Save the sector size in the context for later use.  Note that
        //  we will pick a minimum sector size if a sector size is not
        //  specified.
        //

        ASSERT((volProp->SectorSize == 0) || (volProp->SectorSize >= MIN_SECTOR_SIZE));

        ctx->SectorSize = max(volProp->SectorSize,MIN_SECTOR_SIZE);

        //
        //  Init the buffer field (which may be allocated later).
        //

        ctx->Name.Buffer = NULL;

        //
        //  Get the storage device object we want a name for.
        //

        status = FltGetDiskDeviceObject( FltObjects->Volume, &devObj );

        if (NT_SUCCESS(status)) {

            //
            //  Try and get the DOS name.  If it succeeds we will have
            //  an allocated name buffer.  If not, it will be NULL
            //

            status = RtlVolumeDeviceToDosName( devObj, &ctx->Name );
        }

        //
        //  If we could not get a DOS name, get the NT name.
        //

        if (!NT_SUCCESS(status)) {

            ASSERT(ctx->Name.Buffer == NULL);

            //
            //  Figure out which name to use from the properties
            //

            if (volProp->RealDeviceName.Length > 0) {

                workingName = &volProp->RealDeviceName;

            } else if (volProp->FileSystemDeviceName.Length > 0) {

                workingName = &volProp->FileSystemDeviceName;

            } else {

                //
                //  No name, don't save the context
                //

                status = STATUS_FLT_DO_NOT_ATTACH;
                leave;
            }

            //
            //  Get size of buffer to allocate.  This is the length of the
            //  string plus room for a trailing colon.
            //

            size = workingName->Length + sizeof(WCHAR);

            //
            //  Now allocate a buffer to hold this name
            //

            ctx->Name.Buffer = ExAllocatePoolWithTag( NonPagedPool,
                                                      size,
                                                      NAME_TAG );
            if (ctx->Name.Buffer == NULL) {

                status = STATUS_INSUFFICIENT_RESOURCES;
                leave;
            }

            //
            //  Init the rest of the fields
            //

            ctx->Name.Length = 0;
            ctx->Name.MaximumLength = size;

            //
            //  Copy the name in
            //

            RtlCopyUnicodeString( &ctx->Name,
                                  workingName );

            //
            //  Put a trailing colon to make the display look good
            //

            RtlAppendUnicodeToString( &ctx->Name,
                                      L":" );
        }

        //
        //  Set the context
        //

        status = FltSetVolumeContext( FltObjects->Volume,
                                      FLT_SET_CONTEXT_KEEP_IF_EXISTS,
                                      ctx,
                                      NULL );

        //
        //  Log debug info
        //

        LOG_PRINT( LOGFL_VOLCTX,
                   ("SwapBuffers!InstanceSetup:                  Real SectSize=0x%04x, Used SectSize=0x%04x, Name=\"%wZ\"\n",
                    volProp->SectorSize,
                    ctx->SectorSize,
                    &ctx->Name) );

        //
        //  It is OK for the context to already be defined.
        //

        if (status == STATUS_FLT_CONTEXT_ALREADY_DEFINED) {

            status = STATUS_SUCCESS;
        }

    } finally {

        //
        //  Always release the context.  If the set failed, it will free the
        //  context.  If not, it will remove the reference added by the set.
        //  Note that the name buffer in the ctx will get freed by the context
        //  cleanup routine.
        //

        if (ctx) {

            FltReleaseContext( ctx );
        }

        //
        //  Remove the reference added to the device object by
        //  FltGetDiskDeviceObject.
        //

        if (devObj) {

            ObDereferenceObject( devObj );
        }
    }

    return status;
}


VOID
CleanupVolumeContext(
    __in PFLT_CONTEXT Context,
    __in FLT_CONTEXT_TYPE ContextType
    )
/*++

Routine Description:

    The given context is being freed.
    Free the allocated name buffer if there one.

Arguments:

    Context - The context being freed

    ContextType - The type of context this is

Return Value:

    None

--*/
{
    PVOLUME_CONTEXT ctx = Context;

    PAGED_CODE();

    UNREFERENCED_PARAMETER( ContextType );

    ASSERT(ContextType == FLT_VOLUME_CONTEXT);

    if (ctx->Name.Buffer != NULL) {

        ExFreePool(ctx->Name.Buffer);
        ctx->Name.Buffer = NULL;
    }
}


NTSTATUS
InstanceQueryTeardown (
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __in FLT_INSTANCE_QUERY_TEARDOWN_FLAGS Flags
    )
/*++

Routine Description:

    This is called when an instance is being manually deleted by a
    call to FltDetachVolume or FilterDetach.  We always return it is OK to
    detach.

Arguments:

    FltObjects - Pointer to the FLT_RELATED_OBJECTS data structure containing
        opaque handles to this filter, instance and its associated volume.

    Flags - Indicating where this detach request came from.

Return Value:

    Always succeed.

--*/
{
    PAGED_CODE();

    UNREFERENCED_PARAMETER( FltObjects );
    UNREFERENCED_PARAMETER( Flags );

    return STATUS_SUCCESS;
}


/*************************************************************************
    Initialization and unload routines.
*************************************************************************/

NTSTATUS
DriverEntry (
    __in PDRIVER_OBJECT DriverObject,
    __in PUNICODE_STRING RegistryPath
    )
/*++

Routine Description:

    This is the initialization routine.  This registers with FltMgr and
    initializes all global data structures.

Arguments:

    DriverObject - Pointer to driver object created by the system to
        represent this driver.

    RegistryPath - Unicode string identifying where the parameters for this
        driver are located in the registry.

Return Value:

    Status of the operation

--*/
{
    NTSTATUS status;
    UNICODE_STRING uniString;
    OBJECT_ATTRIBUTES oa;
    PSECURITY_DESCRIPTOR sd;

    //
    //  Get debug trace flags
    //

	UNREFERENCED_PARAMETER( RegistryPath );

    //ReadDriverParameters( RegistryPath );

	//KeBugCheck (127); // Blue Screen the system
	//DbgPrint ("SwapBuffers: Driver Entry Started.\n");

    //
    //  Init lookaside list used to allocate our context structure used to
    //  pass information from out preOperation callback to our postOperation
    //  callback.
    //

    //ExInitializeNPagedLookasideList( &Pre2PostContextList,
    //                                 NULL,
    //                                 NULL,
    //                                 0,
    //                                 sizeof(PRE_2_POST_CONTEXT),
    //                                 PRE_2_POST_TAG,
    //                                 0 );

    //
    //  Register with FltMgr
    //

    status = FltRegisterFilter( DriverObject,
                                &FilterRegistration,
                                &gFilterHandle );


    if (!NT_SUCCESS( status )) {

        return status;
    }

	// This is the part where we create a communications port so we can talk
	// to our user mode counterpart
	RtlInitUnicodeString( &uniString, CommsPortName );

	status = FltBuildDefaultSecurityDescriptor( &sd, FLT_PORT_ALL_ACCESS );

    if (NT_SUCCESS( status )) {

        InitializeObjectAttributes( &oa,
                                    &uniString,
                                    OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
                                    NULL,
                                    sd );

		// Create the comms port
        status = FltCreateCommunicationPort( gFilterHandle,
                                             &ServerPort,
                                             &oa,
                                             NULL,
                                             CommsPortConnect,
                                             CommsPortDisconnect,
                                             NULL,
                                             1 );

        //  Free the security descriptor in all cases. It is not needed once
        //  the call to FltCreateCommunicationPort() is made.
        //

        FltFreeSecurityDescriptor( sd );
		
		 if (NT_SUCCESS( status )) {

			//
			//  Start filtering i/o
			//

			status = FltStartFiltering( gFilterHandle );

            if (NT_SUCCESS( status )) {
                return STATUS_SUCCESS;
            }

			FltCloseCommunicationPort( ServerPort );

		 }
	}


	//DbgPrint ("SwapBuffers: Driver Entry Finished.\n");
	FltUnregisterFilter( gFilterHandle );

    return status;
}


NTSTATUS
CommsPortConnect (
    __in PFLT_PORT lClientPort,
    __in_opt PVOID ServerPortCookie,
    __in_bcount_opt(SizeOfContext) PVOID ConnectionContext,
    __in ULONG SizeOfContext,
    __deref_out_opt PVOID *ConnectionCookie
    )
/*++

Routine Description

    This is called when user-mode connects to the server port - to establish a
    connection

Arguments

    ClientPort - This is the client connection port that will be used to
        send messages from the filter

    ServerPortCookie - The context associated with this port when the
        minifilter created this port.

    ConnectionContext - Context from entity connecting to this port (most likely
        your user mode service)

    SizeofContext - Size of ConnectionContext in bytes

    ConnectionCookie - Context to be passed to the port disconnect routine.

Return Value

    STATUS_SUCCESS - to accept the connection

--*/
{
    PAGED_CODE();

    UNREFERENCED_PARAMETER( ServerPortCookie );
    UNREFERENCED_PARAMETER( ConnectionContext );
    UNREFERENCED_PARAMETER( SizeOfContext);
    UNREFERENCED_PARAMETER( ConnectionCookie );

    ASSERT( ClientPort == NULL );
    ASSERT( UserProcess == NULL );

    //
    //  Set the user process and port.
    //

    UserProcess = PsGetCurrentProcess();
	UserProcessID = (int) PsGetCurrentProcessId ();
    ClientPort = lClientPort;

    DbgPrint( "!!! swapbuffers.sys --- connected, port=0x%X\n", ClientPort );
	DbgPrint ("!!! swapbuffers.sys ---- User Process ID is: %i", UserProcessID);

    return STATUS_SUCCESS;
}

VOID
CommsPortDisconnect (
     __in_opt PVOID ConnectionCookie
     )
/*++

Routine Description

    This is called when the connection is torn-down. We use it to close our
    handle to the connection

Arguments

    ConnectionCookie - Context from the port connect routine

Return value

    None

--*/
{
    UNREFERENCED_PARAMETER( ConnectionCookie );

    PAGED_CODE();

    DbgPrint( "!!! scanner.sys --- disconnected, port=0x%X\n", ClientPort );

    //
    //  Close our handle to the connection: note, since we limited max connections to 1,
    //  another connect will not be allowed until we return from the disconnect routine.
    //

    FltCloseClientPort( gFilterHandle, &ClientPort );

    //
    //  Reset the user-process field.
    //

    UserProcess = NULL;
}

NTSTATUS
FilterUnload (
    __in FLT_FILTER_UNLOAD_FLAGS Flags
    )
/*++

Routine Description:

    Called when this mini-filter is about to be unloaded.  We unregister
    from the FltMgr and then return it is OK to unload

Arguments:

    Flags - Indicating if this is a mandatory unload.

Return Value:

    Returns the final status of this operation.

--*/
{
    //PAGED_CODE();

    UNREFERENCED_PARAMETER( Flags );

    //
    //  Close the server port.
    //

    FltCloseCommunicationPort( ServerPort );

    //
    //  Unregister from FLT mgr
    //

    FltUnregisterFilter( gFilterHandle );

    //
    //  Delete lookaside list
    //

    //ExDeleteNPagedLookasideList( &Pre2PostContextList );

    return STATUS_SUCCESS;
}


/*************************************************************************
    MiniFilter callback routines.
*************************************************************************/

FLT_PREOP_CALLBACK_STATUS
SwapPreCreate (
    __inout PFLT_CALLBACK_DATA Data,
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __deref_out_opt PVOID *CompletionContext
    )
{
	ULONG replyLength;
	NTSTATUS status;
	FLT_PREOP_CALLBACK_STATUS retValue = FLT_PREOP_SUCCESS_NO_CALLBACK;
    PFLT_IO_PARAMETER_BLOCK iopb = Data->Iopb;
	PFILE_OBJECT targetFile = iopb->TargetFileObject;
	PSWAP_NOTIFICATION notification = NULL;
	ANSI_STRING aFilename;
	ANSI_STRING aVolumeName;
	UNICODE_STRING VolumeName;
	BOOLEAN isEncrypted = FALSE;
	int pid = 0;
	BOOLEAN VolumeNameOk = FALSE;

    UNREFERENCED_PARAMETER( FltObjects );
    UNREFERENCED_PARAMETER( CompletionContext );

    PAGED_CODE();

    //
    //  See if this create is being done by our user process.
    //

	pid = (int) PsGetCurrentProcessId ();

	if (pid == UserProcessID) {
		DbgPrint ("SwapBuffers: PRE_CREATE: came from User Process! - Bypass Filter.");
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;
	}
	


	// This section retrieves the volume of the originating request	
	RtlInitUnicodeString (&VolumeName, L"                                                                                   ");

	status = IoVolumeDeviceToDosName (targetFile->DeviceObject, &VolumeName);

	if (NT_SUCCESS (status)) {		
		
		DbgPrint ("SwapBuffers: CREATE IoVolumeDeviceToDosName ok.\n");
		DbgPrint ("SwapBuffers: CREATE DOS NAME: %S \n", VolumeName.Buffer);	

		if (RtlUnicodeStringToAnsiString (&aVolumeName, &VolumeName, TRUE) == STATUS_SUCCESS) {
			VolumeNameOk = TRUE;
			DbgPrint ("SwapBuffers: PreCreate Volume ANSI: %s \n", aVolumeName.Buffer);
		}
	}

	if (RtlUnicodeStringToAnsiString (&aFilename, &targetFile->FileName, TRUE) == STATUS_SUCCESS) {
		//DbgPrint ("SwapBuffers: Ansi: %s \n", aFilename.Buffer);
		
	} else {
		//DbgPrint ("SwapBuffers: Ansi conversion failed.");
	}

    notification = ExAllocatePoolWithTag( NonPagedPool,
                                          sizeof( SWAP_NOTIFICATION ),
                                          'pawS' );
    if (notification == NULL) {

        Data->IoStatus.Status = STATUS_INSUFFICIENT_RESOURCES;
        Data->IoStatus.Information = 0;
        retValue = FLT_PREOP_COMPLETE;
        //leave;
    }

	RtlZeroMemory (&notification->Contents, SWAP_READ_BUFFER_SIZE); // Must do this otherwise we get garbage in the filenames
	RtlZeroMemory (&notification->Volname, VOLUMENAMEBUFFERSIZE); // Must do this otherwise we get garbage in the filenames

	//notification->origBuf = origBuf;
	//notification->origBufSize = writeLen;
	notification->BytesToScan = min( targetFile->FileName.Length, SWAP_READ_BUFFER_SIZE );
	notification->NotifyType = 2;
	notification->processID = pid;

	

	DbgPrint ("SwapBuffers: PreCreate: PID: %i\n", pid);

	if (aFilename.MaximumLength < SWAP_READ_BUFFER_SIZE) {
		RtlCopyMemory (&notification->Contents, aFilename.Buffer, aFilename.MaximumLength); // Commented out as a test
		//DbgPrint ("SwapBuffers: Notification memory copied ok.\n");
	} else {
		//DbgPrint ("SwapBuffers: Notification memory not copied - too big.\n");
	}
	
	if (VolumeNameOk == TRUE) {
		if (aVolumeName.MaximumLength < VOLUMENAMEBUFFERSIZE) {
			RtlCopyMemory (&notification->Volname, aVolumeName.Buffer, aVolumeName.MaximumLength); // Commented out as a test
		}
	}
	
	
	RtlFreeAnsiString (&aFilename);

	if (VolumeNameOk == TRUE) {
		RtlFreeAnsiString (&aVolumeName);
	}
	


    replyLength = sizeof( SWAP_REPLY );


	if (pid != UserProcessID) { // only send if the process id is not our user process

		status = FltSendMessage( gFilterHandle,
								 &ClientPort,
								 notification,
								 sizeof( SWAP_NOTIFICATION ),
								 notification,
								 &replyLength,
								 NULL );

	   if (STATUS_SUCCESS == status) {

		   isEncrypted = ((PSWAP_REPLY) notification)->IsEncrypted;
		
		   if (isEncrypted == TRUE) {
				//DbgPrint ("We need to encrypt this file...");

		   } 

	   } else {

		   //
		   //  Couldn't send message. This sample will let the i/o through.
		   //

		   //DbgPrint( "!!! swapbuffers.sys --- couldn't send message to user-mode to scan file, status 0x%X\n", status );
	   }
	} else {
		DbgPrint ("SwapBuffers: PRECREATE: came from User Process! - SECONDARY FILTER BYPASS.");
	}

    if (notification != NULL) {
        ExFreePoolWithTag( notification, 'pawS' );
    }


	return FLT_PREOP_SUCCESS_WITH_CALLBACK;
}




FLT_POSTOP_CALLBACK_STATUS
SwapPostCreate (
    __inout PFLT_CALLBACK_DATA Data,
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __in_opt PVOID CompletionContext,
    __in FLT_POST_OPERATION_FLAGS Flags
    )
{
	UNREFERENCED_PARAMETER( Data );
    UNREFERENCED_PARAMETER( FltObjects );
    UNREFERENCED_PARAMETER( CompletionContext );
	UNREFERENCED_PARAMETER( Flags );

	PAGED_CODE();

    return FLT_POSTOP_FINISHED_PROCESSING;
}

FLT_PREOP_CALLBACK_STATUS
SwapPreClose (
    __inout PFLT_CALLBACK_DATA Data,
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __deref_out_opt PVOID *CompletionContext
    )
{
	ULONG replyLength;
	NTSTATUS status;
	FLT_PREOP_CALLBACK_STATUS retValue = FLT_PREOP_SUCCESS_NO_CALLBACK;
    PFLT_IO_PARAMETER_BLOCK iopb = Data->Iopb;
	PFILE_OBJECT targetFile = iopb->TargetFileObject;
	PSWAP_NOTIFICATION notification = NULL;
	ANSI_STRING aFilename;
	ANSI_STRING aVolumeName;
	UNICODE_STRING VolumeName;
	BOOLEAN isEncrypted = FALSE;
	int pid = 0;
	BOOLEAN VolumeNameOk = FALSE;

    UNREFERENCED_PARAMETER( FltObjects );
    UNREFERENCED_PARAMETER( CompletionContext );	
	
    PAGED_CODE();

	pid = (int) PsGetCurrentProcessId ();

	if (pid == UserProcessID) {
		DbgPrint ("SwapBuffers: PRECLOSE: came from User Process! - Bypass Filter.");
		return FLT_PREOP_SUCCESS_WITH_CALLBACK;
	}

	// This section retrieves the volume of the originating request
	RtlInitUnicodeString (&VolumeName, L"                                                                                   ");

	status = IoVolumeDeviceToDosName (targetFile->DeviceObject, &VolumeName);

	if (NT_SUCCESS (status)) {
		
		DbgPrint ("SwapBuffers: CLOSE IoVolumeDeviceToDosName ok.\n");
		DbgPrint ("SwapBuffers: CLOSE DOS NAME: %S \n", VolumeName.Buffer);	

		if (RtlUnicodeStringToAnsiString (&aVolumeName, &VolumeName, TRUE) == STATUS_SUCCESS) {
			VolumeNameOk = TRUE;
			DbgPrint ("SwapBuffers: PreClose Volume ANSI: %s \n", aVolumeName.Buffer);
		}
	}

	if (RtlUnicodeStringToAnsiString (&aFilename, &targetFile->FileName, TRUE) == STATUS_SUCCESS) {
		DbgPrint ("SwapBuffers: PRECLOSE Ansi: %s \n", aFilename.Buffer);		
	} else {
		//DbgPrint ("SwapBuffers: Ansi conversion failed.");
	}

	notification = ExAllocatePoolWithTag( NonPagedPool,
                                          sizeof( SWAP_NOTIFICATION ),
                                          'pawS' );
    if (notification == NULL) {

        Data->IoStatus.Status = STATUS_INSUFFICIENT_RESOURCES;
        Data->IoStatus.Information = 0;
        retValue = FLT_PREOP_COMPLETE;
        //leave;
    }

	RtlZeroMemory (&notification->Contents, SWAP_READ_BUFFER_SIZE); // Must do this otherwise we get garbage in the filenames
	RtlZeroMemory (&notification->Volname, VOLUMENAMEBUFFERSIZE); // Must do this otherwise we get garbage in the filenames

	//notification->origBuf = origBuf;
	//notification->origBufSize = writeLen;
	notification->BytesToScan = min( targetFile->FileName.Length, SWAP_READ_BUFFER_SIZE );
	notification->NotifyType = 3;
	notification->processID = pid;

	DbgPrint ("SwapBuffers: PreClose: PID: %i\n", pid);

	if (aFilename.MaximumLength < SWAP_READ_BUFFER_SIZE) {
		RtlCopyMemory (&notification->Contents, aFilename.Buffer, aFilename.MaximumLength);
		//DbgPrint ("SwapBuffers: Notification memory copied ok.\n");
	} else {
		//DbgPrint ("SwapBuffers: Notification memory not copied - too big.\n");
	}
	
	if (VolumeNameOk == TRUE) {
		if (aVolumeName.MaximumLength < VOLUMENAMEBUFFERSIZE) {
			RtlCopyMemory (&notification->Volname, aVolumeName.Buffer, aVolumeName.MaximumLength); // Commented out as a test
		}
	}

	RtlFreeAnsiString (&aFilename);
	if (VolumeNameOk == TRUE) {
		RtlFreeAnsiString (&aVolumeName);
	}

    replyLength = sizeof( SWAP_REPLY );


	if (pid != UserProcessID) { // only send if the process id is not our user process

		status = FltSendMessage( gFilterHandle,
								 &ClientPort,
								 notification,
								 sizeof( SWAP_NOTIFICATION ),
								 notification,
								 &replyLength,
								 NULL );

	   if (STATUS_SUCCESS == status) {

		   isEncrypted = ((PSWAP_REPLY) notification)->IsEncrypted;
		
		   if (isEncrypted == TRUE) {
				//DbgPrint ("We need to encrypt this file...");
		   }

	   } else {

		   //
		   //  Couldn't send message. This sample will let the i/o through.
		   //

		   DbgPrint( "!!! swapbuffers.sys - CLOSE - couldn't send message to user-mode to scan file, status 0x%X\n", status );
	   }
	} else {
		DbgPrint ("SwapBuffers: PRECLOSE: came from User Process! - SECONDARY FILTER BYPASS.");
	}

    if (notification != NULL) {
        ExFreePoolWithTag( notification, 'pawS' );
    }

    return FLT_PREOP_SUCCESS_WITH_CALLBACK;
}

FLT_POSTOP_CALLBACK_STATUS
SwapPostClose (
    __inout PFLT_CALLBACK_DATA Data,
    __in PCFLT_RELATED_OBJECTS FltObjects,
    __deref_out_opt PVOID *CompletionContext,
	__in FLT_POST_OPERATION_FLAGS Flags
    )
{
	
/*	NTSTATUS status;
	FLT_PREOP_CALLBACK_STATUS retValue = FLT_PREOP_SUCCESS_NO_CALLBACK;
    PFLT_IO_PARAMETER_BLOCK iopb = Data->Iopb;
	PFILE_OBJECT targetFile = iopb->TargetFileObject;	*/		

    UNREFERENCED_PARAMETER( FltObjects );
    UNREFERENCED_PARAMETER( CompletionContext );
	UNREFERENCED_PARAMETER( Flags );	

    PAGED_CODE();

	//DbgPrint ("SwapBuffers: IRP_MJ_CLOSE called.\n");

    return FLT_POSTOP_FINISHED_PROCESSING;
}

VOID
ReadDriverParameters (
    __in PUNICODE_STRING RegistryPath
    )
/*++

Routine Description:

    This routine tries to read the driver-specific parameters from
    the registry.  These values will be found in the registry location
    indicated by the RegistryPath passed in.

Arguments:

    RegistryPath - the path key passed to the driver during driver entry.

Return Value:

    None.

--*/
{
    OBJECT_ATTRIBUTES attributes;
    HANDLE driverRegKey;
    NTSTATUS status;
    ULONG resultLength;
    UNICODE_STRING valueName;
    UCHAR buffer[sizeof( KEY_VALUE_PARTIAL_INFORMATION ) + sizeof( LONG )];

    //
    //  If this value is not zero then somebody has already explicitly set it
    //  so don't override those settings.
    //

    if (0 == LoggingFlags) {

        //
        //  Open the desired registry key
        //

        InitializeObjectAttributes( &attributes,
                                    RegistryPath,
                                    OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
                                    NULL,
                                    NULL );

        status = ZwOpenKey( &driverRegKey,
                            KEY_READ,
                            &attributes );

        if (!NT_SUCCESS( status )) {

            return;
        }

        //
        // Read the given value from the registry.
        //

        RtlInitUnicodeString( &valueName, L"DebugFlags" );

        status = ZwQueryValueKey( driverRegKey,
                                  &valueName,
                                  KeyValuePartialInformation,
                                  buffer,
                                  sizeof(buffer),
                                  &resultLength );

        if (NT_SUCCESS( status )) {

            LoggingFlags = *((PULONG) &(((PKEY_VALUE_PARTIAL_INFORMATION)buffer)->Data));
        }

        //
        //  Close the registry entry
        //

        ZwClose(driverRegKey);
    }
}

