#pragma once

#include <windows.h>
#include "resource.h"
#include "DynList.h"
#include "MemoryBuffer.h"
#include "StringItem.h"

class CommonFunctions {
	
	public:
		CommonFunctions ();
		~CommonFunctions ();
		
		void SIDLToFile (DynList *pdl, char *szFilename);
		void FileToSIDL (DynList *pdl, char *szFilename);
		void GetPathOnly (char *szInpath, char *szOutpath);
		bool FileExists (char *szFilepath);
		bool LocalFileExists (char *szFilename);
		void ListBoxToSIDL (HWND hwndlistbox, DynList *pdl);
		void SIDLToListBox (HWND hwndlistbox, DynList *pdl);
		bool DoesListBoxStringExist (HWND hwndlistbox, char *szItem);
		bool DoesSIExist (DynList *pdl, char *szItem);
	private:
		


};
