// This work is dedicated to the Lord our God. King of Heaven, Lord of Heaven's Armies.

#include "OptionsWindow.h"

OptionsWindow::OptionsWindow ()
{
	m_bUseDiagnostics = true;
	ZeroMemory (m_szClassname, SIZE_STRING);
	m_ID = 0;
	m_bInitialised = false;
	m_bincludefoldersonly = false;
	m_bautostart = true;
	m_busealerts = true;
}

OptionsWindow::~OptionsWindow ()
{

}

void OptionsWindow::Initialise (HWND hWnd, unsigned int uID)
{	
	// Make the ID global
	m_ID = uID;
	m_parenthwnd = hWnd;
	//m_hwnd = hWnd;

	// Temporary string value for uID
	char szID[SIZE_STRING];
	ZeroMemory (szID, SIZE_STRING);

	SetParentHWND (hWnd);
	SetBgColor (GetSysColor (COLOR_BTNFACE));
	SetCaption (TEXT ("CedeCrypt Realtime Options"));
	SetWindowStyle (FS_STYLESTANDARD);

	// Create the class name
	strcat_s (m_szClassname, SIZE_STRING, "OPTWindowClass");
	sprintf_s (szID, SIZE_STRING, "%d", uID);
	strcat_s (m_szClassname, SIZE_STRING, szID);

	if (m_bInitialised == false) {
		m_bInitialised = true;
		CreateAppWindow (m_szClassname, 70, 0, 550, 500, true);
	}
	
	SetWindowPosition (FS_CENTER);	
	Show ();
}

void OptionsWindow::OnDestroy (HWND hWnd)
{			
	Hide ();
}

void OptionsWindow::OnCreate (HWND hWnd)
{				
	// Make this window handle global
	m_hwnd = hWnd;	
	g_hWnd = hWnd;
		

	m_lstoptpanel = CreateListBox (10, 10, 100, 480, ID_LSTOPTPANEL);
	SetListBoxItemHeight (m_lstoptpanel, 32);

	SendMessage (m_lstoptpanel, LB_ADDSTRING, 0, (LPARAM) "General");
	SendMessage (m_lstoptpanel, LB_ADDSTRING, 0, (LPARAM) "File Types");
	SendMessage (m_lstoptpanel, LB_ADDSTRING, 0, (LPARAM) "Applications");
	SendMessage (m_lstoptpanel, LB_ADDSTRING, 0, (LPARAM) "Folders");

	
	// Create the General panel
	CreateGeneralPanel ();
	CreateFileTypesPanel ();
	CreateApplicationsPanel ();
	CreateFoldersPanel ();

	SetFoldersPanelVisible (false);
	SetApplicationsPanelVisible (false);
	SetFileTypesPanelVisible (false);
	SetGeneralPanelVisible (true);

	SendMessage (m_lstoptpanel, LB_SETCURSEL, 0, 0);

	RefreshIncludeFolders ();
	RefreshIndividualSettings ();

	m_btnclose = CreateButton ("Close", 455, 440, 70, 23, ID_OPTWINDOWCLOSE);
}

void OptionsWindow::SetDefaultDirs (DynList *pdlexdir)
{
	m_pdldefault = pdlexdir;
}

void OptionsWindow::SetConfig (DynList *pdlextensions, DynList *pdlapps, DynList *pdlexdir, DynList *pdlindir, UserSettings *psettings)
{
	m_cmf.SIDLToListBox (m_lstfiletypes, pdlextensions);
	m_cmf.SIDLToListBox (m_lstapplications, pdlapps);
	m_cmf.SIDLToListBox (m_lstexcludedfolders, pdlexdir);
	m_cmf.SIDLToListBox (m_lstincludedfolders, pdlindir);

	m_bincludefoldersonly = psettings->bUseIncludeFolders;
	m_bautostart = psettings->bAutoStart;
	m_busealerts = psettings->bUseAlerts;

	RefreshIncludeFolders ();
	RefreshIndividualSettings ();


}

void OptionsWindow::GetConfig (DynList *pdlextensions, DynList *pdlapps, DynList *pdlexdir, DynList *pdlindir, UserSettings *psettings)
{
	m_cmf.ListBoxToSIDL (m_lstfiletypes, pdlextensions);
	m_cmf.ListBoxToSIDL (m_lstapplications, pdlapps);
	m_cmf.ListBoxToSIDL (m_lstexcludedfolders, pdlexdir);
	m_cmf.ListBoxToSIDL (m_lstincludedfolders, pdlindir);

	psettings->bUseIncludeFolders = m_bincludefoldersonly;
	psettings->bAutoStart = m_bautostart;
	psettings->bUseAlerts = m_busealerts;

}

void OptionsWindow::CreateGeneralPanel ()
{
	m_grpgeneral = CreateGroupBox ("General Options", 130, 15, 390, 413, ID_OPTSTATIC);

	m_chkstartup = CreateCheckBox ("Start CedeCrypt Realtime Encryption automatically.", 170, 50, 300, 25, ID_CHKSTARTUP);
	m_chkalerts = CreateCheckBox ("Show CedeCrypt alerts should a problem occur", 170, 80, 300, 30, ID_CHKALERTS);

}

void OptionsWindow::SetGeneralPanelVisible (bool bVisible)
{
	if (bVisible == true) {
		ShowWindow (m_grpgeneral, SW_SHOW);
		ShowWindow (m_chkstartup, SW_SHOW);
		ShowWindow (m_chkalerts, SW_SHOW);
	} else {
		ShowWindow (m_grpgeneral, SW_HIDE);
		ShowWindow (m_chkstartup, SW_HIDE);
		ShowWindow (m_chkalerts, SW_HIDE);
	}
}

void OptionsWindow::CreateFileTypesPanel ()
{
	m_grpfiletypes = CreateGroupBox ("File Type Options", 130, 15, 390, 413, ID_OPTSTATIC);

	m_lblftstatic = CreateLabel ("The following file types will be automatically Encrypted and Decrypted in realtime. To include additional File Types, type the file extension below, and click 'Add'", 170, 80, 300, 40, ID_LBLSTATIC);	
	m_lstfiletypes = CreateListBox (240, 130, 170, 190, ID_LSTFILETYPES);
	m_txtnewext = CreateTextBox (240, 305, 110, 20, ID_TXTNEWEXT);
	m_btnadd = CreateButton ("Add", 352, 305, 57, 20, ID_BTNADDEXT);
	m_btnremove = CreateButton ("Remove", 413, 130, 50, 20, ID_BTNREMOVEEXT);
}

void OptionsWindow::SetFileTypesPanelVisible (bool bVisible)
{
	if (bVisible == true) {
		ShowWindow (m_grpfiletypes, SW_SHOW);
		ShowWindow (m_lblftstatic, SW_SHOW);
		ShowWindow (m_lstfiletypes, SW_SHOW);
		ShowWindow (m_txtnewext, SW_SHOW);
		ShowWindow (m_btnadd, SW_SHOW);
		ShowWindow (m_btnremove, SW_SHOW);
	} else {
		ShowWindow (m_grpfiletypes, SW_HIDE);
		ShowWindow (m_lblftstatic, SW_HIDE);
		ShowWindow (m_lstfiletypes, SW_HIDE);
		ShowWindow (m_txtnewext, SW_HIDE);
		ShowWindow (m_btnadd, SW_HIDE);
		ShowWindow (m_btnremove, SW_HIDE);	
	}
}

void OptionsWindow::CreateApplicationsPanel ()
{

	m_grpapplications = CreateGroupBox ("Application Options", 130, 15, 390, 413, ID_OPTSTATIC);

	m_lblappstatic = CreateLabel ("The following applications will be excluded from the Realtime Encryption Engine. If one of the following applications requests an Encrypted file, no realtime encryption or decryption will take place.", 170, 65, 300, 55, ID_LBLSTATIC);	

	m_lstapplications = CreateListBox (240, 130, 170, 190, ID_LSTAPPLICATIONS);
	m_txtnewapp = CreateTextBox (240, 305, 110, 20, ID_TXTNEWAPP);
	m_btnaddapp = CreateButton ("Add", 352, 305, 57, 20, ID_BTNADDAPP);
	m_btnremoveapp = CreateButton ("Remove", 413, 130, 50, 20, ID_BTNREMOVEAPP);
}

void OptionsWindow::SetApplicationsPanelVisible (bool bVisible)
{
	if (bVisible == true) {
		ShowWindow (m_grpapplications, SW_SHOW);
		ShowWindow (m_lblappstatic, SW_SHOW);
		ShowWindow (m_lstapplications, SW_SHOW);
		ShowWindow (m_txtnewapp, SW_SHOW);
		ShowWindow (m_btnaddapp, SW_SHOW);
		ShowWindow (m_btnremoveapp, SW_SHOW);
	} else {
		ShowWindow (m_grpapplications, SW_HIDE);
		ShowWindow (m_lblappstatic, SW_HIDE);
		ShowWindow (m_lstapplications, SW_HIDE);
		ShowWindow (m_txtnewapp, SW_HIDE);
		ShowWindow (m_btnaddapp, SW_HIDE);
		ShowWindow (m_btnremoveapp, SW_HIDE);	
	}
	
}

void OptionsWindow::CreateFoldersPanel ()
{
	m_grpfolders = CreateGroupBox ("Folder Options", 130, 15, 390, 413, ID_OPTSTATIC);
	
	m_lblfolderexclusionsstatic = CreateLabel ("The following folders will be excluded from the Realtime Encryption Engine. By Default application and system folders are not encrypted.", 145, 40, 355, 28, ID_LBLSTATIC);	
	m_lstexcludedfolders = CreateListBox (145, 75, 360, 150, ID_LSTEXCLUDEDFOLDERS);
	m_btnaddexclusion = CreateButton ("Add", 405, 225, 50, 20, ID_BTNADDEXCLUSION);
	m_btnremoveexclusion = CreateButton ("Remove", 460, 225, 50, 20, ID_BTNREMOVEEXCLUSION);

	m_chkincludeonly = CreateCheckBox ("Include only the following folders...", 145, 235, 200, 19, ID_CHKINCLUDEONLY);

	m_lblfolderinclusionstatic = CreateLabel ("The following folders will be the only folders encrypted and decrypted by the Realtime Encryption Engine. Exclusions do not apply.", 145, 260, 355, 28, ID_LBLSTATIC);	
	m_lstincludedfolders = CreateListBox (145, 293, 360, 100, ID_LSTINCLUDEDFOLDERS);

	m_btnaddinclusion = CreateButton ("Add", 405, 395, 50, 20, ID_BTNADDINCLUSION);
	m_btnremoveinclusion = CreateButton ("Remove", 460, 395, 50, 20, ID_BTNREMOVEINCLUSION);
}

void OptionsWindow::SetFoldersPanelVisible (bool bVisible)
{
	if (bVisible == true) {
		ShowWindow (m_grpfolders, SW_SHOW);
		ShowWindow (m_lblfolderexclusionsstatic, SW_SHOW);
		ShowWindow (m_lstexcludedfolders, SW_SHOW);
		ShowWindow (m_btnaddexclusion, SW_SHOW);
		ShowWindow (m_btnremoveexclusion, SW_SHOW);
		ShowWindow (m_chkincludeonly, SW_SHOW);
		ShowWindow (m_lblfolderinclusionstatic, SW_SHOW);
		ShowWindow (m_lstincludedfolders, SW_SHOW);
		ShowWindow (m_btnaddinclusion, SW_SHOW);
		ShowWindow (m_btnremoveinclusion, SW_SHOW);
	} else {
		ShowWindow (m_grpfolders, SW_HIDE);
		ShowWindow (m_lblfolderexclusionsstatic, SW_HIDE);
		ShowWindow (m_lstexcludedfolders, SW_HIDE);
		ShowWindow (m_btnaddexclusion, SW_HIDE);
		ShowWindow (m_btnremoveexclusion, SW_HIDE);
		ShowWindow (m_chkincludeonly, SW_HIDE);
		ShowWindow (m_lblfolderinclusionstatic, SW_HIDE);
		ShowWindow (m_lstincludedfolders, SW_HIDE);
		ShowWindow (m_btnaddinclusion, SW_HIDE);
		ShowWindow (m_btnremoveinclusion, SW_HIDE);	
	}
}

void OptionsWindow::RefreshIncludeFolders ()
{
	// Checks if the include option has been set for folders, and greys out the
	// relevant set of controls. Basically either exclusions OR inclusions must be
	// enabled, not both.

	if (m_bincludefoldersonly == true) {
		// Disable exclusions section
		EnableWindow (m_lblfolderexclusionsstatic, FALSE);
		EnableWindow (m_lstexcludedfolders, FALSE);
		EnableWindow (m_btnaddexclusion, FALSE);
		EnableWindow (m_btnremoveexclusion, FALSE);

		EnableWindow (m_lblfolderinclusionstatic, TRUE);
		EnableWindow (m_lstincludedfolders, TRUE);
		EnableWindow (m_btnaddinclusion, TRUE);
		EnableWindow (m_btnremoveinclusion, TRUE);

		SetCheck (m_chkincludeonly, true);
	} else {
		EnableWindow (m_lblfolderexclusionsstatic, TRUE);
		EnableWindow (m_lstexcludedfolders, TRUE);
		EnableWindow (m_btnaddexclusion, TRUE);
		EnableWindow (m_btnremoveexclusion, TRUE);

		EnableWindow (m_lblfolderinclusionstatic, FALSE);
		EnableWindow (m_lstincludedfolders, FALSE);
		EnableWindow (m_btnaddinclusion, FALSE);
		EnableWindow (m_btnremoveinclusion, FALSE);	

		SetCheck (m_chkincludeonly, false);
	}
}

void OptionsWindow::RefreshIndividualSettings ()
{	
	if (m_bautostart == true) {
		SetCheck (m_chkstartup, true);		
	} else {			
		SetCheck (m_chkstartup, false);		
	}
			
	if (m_busealerts == true) {
		SetCheck (m_chkalerts, true);		
	} else {			
		SetCheck (m_chkalerts, false);		
	}		
}

void OptionsWindow::ActivatePanel (char *szPanelname)
{
	//SendMessage (m_lstoptpanel, LB_ADDSTRING, 0, (LPARAM) "General");
	//SendMessage (m_lstoptpanel, LB_ADDSTRING, 0, (LPARAM) "File Types");
	//SendMessage (m_lstoptpanel, LB_ADDSTRING, 0, (LPARAM) "Applications");
	//SendMessage (m_lstoptpanel, LB_ADDSTRING, 0, (LPARAM) "Folders");

	if (strcmp (szPanelname, "General") == 0) {
		SetFoldersPanelVisible (false);
		SetApplicationsPanelVisible (false);
		SetFileTypesPanelVisible (false);
		SetGeneralPanelVisible (true);
	}

	if (strcmp (szPanelname, "File Types") == 0) {
		SetFoldersPanelVisible (false);
		SetApplicationsPanelVisible (false);
		SetFileTypesPanelVisible (true);
		SetGeneralPanelVisible (false);
	}

	if (strcmp (szPanelname, "Applications") == 0) {
		SetFoldersPanelVisible (false);
		SetApplicationsPanelVisible (true);
		SetFileTypesPanelVisible (false);
		SetGeneralPanelVisible (false);
	}
	
	if (strcmp (szPanelname, "Folders") == 0) {
		SetFoldersPanelVisible (true);
		SetApplicationsPanelVisible (false);
		SetFileTypesPanelVisible (false);
		SetGeneralPanelVisible (false);
	}
}

bool OptionsWindow::BrowseForFolder (LPCTSTR szTitle, char *szOutPath) {
	CoInitialize (NULL);
	BROWSEINFO bi = { 0 };
	bi.lpszTitle = szTitle;
	bi.ulFlags = BIF_USENEWUI;
	LPITEMIDLIST pidl = SHBrowseForFolder ( &bi );
	if ( pidl != 0 ) {
		// get the name of the folder
		SHGetPathFromIDList (pidl, szOutPath);
		// free memory used
		IMalloc * imalloc = 0;
		if ( SUCCEEDED( SHGetMalloc ( &imalloc )) ) {
			imalloc->Free ( pidl );
			imalloc->Release ( );
		}
		return true;
	}
	return false;
}

void OptionsWindow::OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	
	switch (LOWORD (wParam)) {

		case ID_CHKSTARTUP:
		{			
			if (m_bautostart == true) {
				SetCheck (m_chkstartup, false);
				m_bautostart = false;
			} else {			
				SetCheck (m_chkstartup, true);
				m_bautostart = true;
			}
		}
		break;

		case ID_CHKALERTS:
		{			
			if (m_busealerts == true) {
				SetCheck (m_chkalerts, false);
				m_busealerts = false;
			} else {			
				SetCheck (m_chkalerts, true);
				m_busealerts = true;
			}
		}
		break;

		case ID_CHKINCLUDEONLY:
		{
			if (m_bincludefoldersonly == true) {
				SetCheck (m_chkincludeonly, false);
				m_bincludefoldersonly = false;
			} else {
				SetCheck (m_chkincludeonly, true);
				m_bincludefoldersonly = true;
			}

			RefreshIncludeFolders ();
		}
		break;
		case ID_BTNADDINCLUSION:
		{
			char szFolder[SIZE_STRING];
			ZeroMemory (szFolder, SIZE_STRING);

			char szShortpath[SIZE_STRING];
			ZeroMemory (szShortpath, SIZE_STRING);

			BrowseForFolder ("Locate a folder to include...", szFolder);

			if (strlen (szFolder) > 3) {								
				if (m_cmf.DoesListBoxStringExist (m_lstincludedfolders, szFolder) == false) {
					SendMessage (m_lstincludedfolders, LB_ADDSTRING, 0, (LPARAM) szFolder);

					// Now add the short path
					if (GetShortPathName (szFolder, szShortpath, SIZE_STRING) != 0) {
						// Success on get short path

						_strupr (szFolder);
						_strupr (szShortpath);

						// Now check that the long path and short path are different
						if (strcmp (szFolder, szShortpath) != 0) {
							SendMessage (m_lstincludedfolders, LB_ADDSTRING, 0, (LPARAM) szShortpath);
						}
					}

				} else {
					MessageBox (hWnd, "That folder is already in the list.", "Folder Present", MB_OK | MB_ICONEXCLAMATION);				
				}			
			}
		}
		break;

		case ID_BTNREMOVEINCLUSION:
		{
			int iselindex = SendMessage (m_lstincludedfolders, LB_GETCURSEL, 0, 0);

			if (iselindex != LB_ERR) {								
				SendMessage (m_lstincludedfolders, LB_DELETESTRING, iselindex, 0);			
			}
		}
		break;

		case ID_BTNADDEXCLUSION:
		{
			char szFolder[SIZE_STRING];
			ZeroMemory (szFolder, SIZE_STRING);

			char szShortpath[SIZE_STRING];
			ZeroMemory (szShortpath, SIZE_STRING);

			BrowseForFolder ("Locate a folder to exclude...", szFolder);

			if (strlen (szFolder) > 3) {								
				if (m_cmf.DoesListBoxStringExist (m_lstexcludedfolders, szFolder) == false) {
					SendMessage (m_lstexcludedfolders, LB_ADDSTRING, 0, (LPARAM) szFolder);

					// Now add the short path
					if (GetShortPathName (szFolder, szShortpath, SIZE_STRING) != 0) {
						// Success on get short path

						_strupr (szFolder);
						_strupr (szShortpath);

						// Now check that the long path and short path are different
						if (strcmp (szFolder, szShortpath) != 0) {
							SendMessage (m_lstexcludedfolders, LB_ADDSTRING, 0, (LPARAM) szShortpath);
						}
					}

				} else {
					MessageBox (hWnd, "That folder is already in the list.", "Folder Present", MB_OK | MB_ICONEXCLAMATION);				
				}			
			}
		}
		break;

		case ID_BTNREMOVEEXCLUSION:
		{
			char szCurrent[SIZE_STRING];
			ZeroMemory (szCurrent, SIZE_STRING);
			int iselindex = SendMessage (m_lstexcludedfolders, LB_GETCURSEL, 0, 0);

			if (iselindex != LB_ERR) {
				
				SendMessage (m_lstexcludedfolders, LB_GETTEXT, iselindex, (LPARAM) szCurrent);

				if (m_cmf.DoesSIExist (m_pdldefault, szCurrent) == true) { // is this s default filtered excluded dir
					// If yes, then we can't allow the user to remove it.
					MessageBox (hWnd, "This is a default excluded folder and cannot be removed. Application and System folders must be excluded from the Realtime Encryption Engine.", "Default Folder", MB_OK | MB_ICONEXCLAMATION);
				
				} else {
					SendMessage (m_lstexcludedfolders, LB_DELETESTRING, iselindex, 0);
				}

				
			}
		}
		break;

		case ID_BTNADDAPP:
		{
			//ID_TXTNEWAPP
			char szText[SIZE_STRING];
			ZeroMemory (szText, SIZE_STRING);
			GetDlgItemText (hWnd, ID_TXTNEWAPP, szText, SIZE_STRING);

			if (strlen (szText) > 3) {				

				SetDlgItemText (hWnd, ID_TXTNEWAPP, ""); // Clear the text box

				if (m_cmf.DoesListBoxStringExist (m_lstapplications, szText) == false) {
					SendMessage (m_lstapplications, LB_ADDSTRING, 0, (LPARAM) szText);
				} else {
					MessageBox (hWnd, "That application is already in the list.", "Application Present", MB_OK | MB_ICONEXCLAMATION);				
				}								
			} else {
				MessageBox (hWnd, "Please enter a valid application, e.g. 'WINWORD.EXE'.", "Input Validation", MB_OK | MB_ICONEXCLAMATION);
			}
		}
		break;

		case ID_BTNREMOVEAPP:
		{
			int iselindex = SendMessage (m_lstapplications, LB_GETCURSEL, 0, 0);

			if (iselindex != LB_ERR) {
				SendMessage (m_lstapplications, LB_DELETESTRING, iselindex, 0);
			}
		}
		break;



		case ID_BTNADDEXT:
		{
			//ID_TXTNEWEXT
			char szText[SIZE_STRING];
			ZeroMemory (szText, SIZE_STRING);
			GetDlgItemText (hWnd, ID_TXTNEWEXT, szText, SIZE_STRING);

			if (strlen (szText) > 3) {				

				SetDlgItemText (hWnd, ID_TXTNEWEXT, ""); // Clear the text box

				if (m_cmf.DoesListBoxStringExist (m_lstfiletypes, szText) == false) {
					SendMessage (m_lstfiletypes, LB_ADDSTRING, 0, (LPARAM) szText);
				} else {
					MessageBox (hWnd, "That extension is already in the list.", "Extension Present", MB_OK | MB_ICONEXCLAMATION);				
				}								
			} else {
				MessageBox (hWnd, "Please enter a valid extension, e.g. '.xls'.", "Input Validation", MB_OK | MB_ICONEXCLAMATION);
			}
		}
		break;
		case ID_BTNREMOVEEXT:
		{
			int iselindex = SendMessage (m_lstfiletypes, LB_GETCURSEL, 0, 0);

			if (iselindex != LB_ERR) {
				SendMessage (m_lstfiletypes, LB_DELETESTRING, iselindex, 0);
			}
		}
		break;
		case ID_OPTWINDOWCLOSE:
		{
			SendMessage (m_parenthwnd, WM_OPTIONSCLOSED, 0, 0);
			ShowWindow (m_hwnd, SW_HIDE);
			
		}
		break;
		case ID_LSTOPTPANEL:
		{			
			switch (HIWORD (wParam))
			{
				case LBN_SELCHANGE:
				{					
					int iIndex = SendMessage (m_lstoptpanel, LB_GETCURSEL, 0, 0);
					char selItem[SIZE_STRING];
					ZeroMemory (selItem, SIZE_STRING);

					if (iIndex != LB_ERR) {
						SendMessage (m_lstoptpanel,LB_GETTEXT, iIndex, (LPARAM) selItem);

						ActivatePanel (selItem);		
					}
				}
				break;
			}
			break;
		}
		break;

	}
}


void OptionsWindow::OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam)
{

}

void OptionsWindow::OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam)
{
	switch (wParam)
	{

	}
}

void OptionsWindow::OnTimer (WPARAM wParam)
{	

}

void OptionsWindow::OnPaint (HWND hWnd)
{

}

void OptionsWindow::OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos)
{	

}

void OptionsWindow::OnLButtonDown (HWND hWnd)
{

}

void OptionsWindow::OnLButtonUp (HWND hWnd)
{

}