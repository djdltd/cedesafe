//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ScanUserWin.rc

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

#define IDI_ICON                        1
#define IDI_ICONSMALL                   2

#define ICONNORM	100
#define	ICONRIGHT	101
#define	ICONTOP		102
#define	ICONBOTTOM	103
#define	ICONLEFT	104
#define ICONERROR	105

#define IDB_PASSBANNER	105
#define IDB_ALERTBANNER	106

#define SIZE_SMALLINT                   16
#define SIZE_INTEGER                    32
#define SIZE_NAME                       64
#define SIZE_STRING						1024
#define FS_CENTER                       100
#define FS_BOTTOMRIGHT                  101
#define FS_STYLESTANDARD                102
#define FS_STYLEBLANK                   103
#define WM_UICOMMAND                    8000
#define WM_UIHIGHLIGHT                  8001
#define WM_UINOHIGHLIGHT                8002
#define WM_MESSAGINGEVENT               8005
#define WM_UISCROLL                     8006
#define WM_ACTIVATEALERT				8007
#define WM_STARTTRAYANIM				8008
#define WM_STOPTRAYANIM					8009
#define WM_OPTIONSCLOSED				8010
#define CID_PASSWORDOK                  415
#define CID_PASSWORDCANCEL              416

#define WM_SYSTRAY						2000
#define IDM_TRAY_SHOWLOG				2001
#define IDM_TRAY_ABOUT					2002
#define IDM_TRAY_SHUTDOWN				2003
#define IDM_TRAY_ENCLOG					2004
#define IDM_TRAY_ENABLE					2005
#define IDM_TRAY_DISABLE				2006
#define IDM_TRAY_ENTERPASSWORD			2007
#define IDM_TRAY_OPTIONS				2008

#define ID_DIAGLIST						400
#define ID_TESTBUTTON					401
#define ID_TESTBUTTON2					402
#define ID_BTNLISTEVENTS				403
#define IDT_TIMERTRAYANIMFULL			404
#define IDT_TIMERTRAYANIMSTOP			405
#define IDT_ALERTANIMSHOW				406
#define IDT_ALERTANIMHIDE				407

// Option controls
#define ID_LSTOPTPANEL					408
#define ID_OPTWINDOWCLOSE				409
#define ID_OPTSTATIC					410
#define ID_CHKSTARTUP					411
#define ID_CHKALERTS					412
// File Types Panel
#define ID_LSTFILETYPES					414
#define ID_TXTNEWEXT					415
#define ID_BTNADDEXT					416
#define ID_BTNREMOVEEXT					417
// Applications Panel
#define ID_LSTAPPLICATIONS				418
#define ID_TXTNEWAPP					419
#define ID_BTNADDAPP					420
#define ID_BTNREMOVEAPP					421
// Folders Panel
#define ID_LSTEXCLUDEDFOLDERS			422
#define ID_BTNADDEXCLUSION				423
#define ID_BTNREMOVEEXCLUSION			424
#define ID_CHKINCLUDEONLY				425
#define ID_LSTINCLUDEDFOLDERS			426
#define ID_BTNADDINCLUSION				427
#define ID_BTNREMOVEINCLUSION			428
#define ID_BTNSHOWSAMPLEALERT			429

#define WM_ADDENCRYPTEVENT				4000
#define WM_GENEVENTID					4001
#define WM_ENGINERESTART				4002

#define IDC_SYSTRAYICON					4003

#define CRYPTRES_FAILED					100
#define CRYPTRES_NOTENCRYPTED			101
#define CRYPTRES_ENCRYPTED				102

#define MAXFIXEDEVENTS					250

#define CRYPT_MSG                       903

#define ID_EDITPASSWORD					5000
#define ID_LBLSESSIONINFO				5001
#define ID_BTNPASSOK					5002
#define ID_BTNPASSCANCEL				5003
#define ID_LBLSTATIC					5004
#define ID_EDITPASSWORD2				5005
#define ID_LBLSTATIC2					5006
#define ID_LBLSTATIC3					5007
#define ID_LBLALERT						5008

#define IDD_ALERTPOPUP					5009
#define IDD_ABOUTDIALOG					5010
#define IDC_ABOUTSTATIC					5011
#define IDC_ABOUTSTATIC2				5012
#define IDD_ENCLOGDIALOG				101
