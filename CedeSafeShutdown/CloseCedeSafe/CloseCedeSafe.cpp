#define _WIN32_WINNT 0x0501

#include <windows.h>
#include "psapi.h"

#define SIZE_STRING		1024
#define SIZE_NAME			64

// Globals
HWND g_hWnd = NULL;
const char g_szClassName[] = "CedeSafeShutdownWindowClass";
UINT g_ipcmessage;

LRESULT CALLBACK WndProc (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch (msg) {
		
		case WM_CREATE:
		{
			g_hWnd = hwnd;
			g_ipcmessage = RegisterWindowMessage ("CedeSafe Quick IPC");

			SendMessage (HWND_BROADCAST, g_ipcmessage, 0, 0);

			PostQuitMessage (0);
		}
		break;
		case WM_COMMAND:
		{
			switch (wParam)
			{
				
			}
		}
		break;
		case WM_TIMER:
		{
			switch (wParam)
			{

			}
		}
		break;
		case WM_CLOSE:
			DestroyWindow (hwnd);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;		
		default:
			return DefWindowProc (hwnd, msg, wParam, lParam);
	}
	return 0;
}


int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) 
{
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;	
	
	// Step1: Registering the window class
	wc.cbSize = sizeof (WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0L;
	wc.cbWndExtra = 0L;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon (hInstance, IDI_APPLICATION);
	wc.hCursor = LoadCursor (NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH) CreateSolidBrush (RGB (236, 233, 216));
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm = LoadIcon (hInstance, IDI_APPLICATION);

	if (!RegisterClassEx (&wc)) {
		MessageBox (NULL, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	// Step2: Creating the window
	hwnd = CreateWindowEx (NULL, g_szClassName, "CedeSafe Shutdown", 
					WS_MINIMIZEBOX | WS_BORDER, 300, 50, 250, 50,
					NULL, NULL, hInstance, NULL);

	if (hwnd == NULL) {
		MessageBox (NULL, "Window creation failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	//ShowWindow (hwnd, nCmdShow);
	//UpdateWindow (hwnd);

	// Step3: The message loop	
	while (GetMessage (&Msg, NULL, 0, 0) > 0) {
		TranslateMessage (&Msg);
		DispatchMessage (&Msg);
	}

	return Msg.wParam;
	return 0;
}