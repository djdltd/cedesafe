;NSIS Modern User Interface
;Start Menu Folder Selection Example Script
;Written by Joost Verburg

;--------------------------------
;Include Modern UI

  !include "MUI.nsh"

;--------------------------------
;General

  SetCompress auto
  CRCCheck on
  ;Name and file
  Name "CedeSafe"
  OutFile "CedeSafeSetup.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES\CedeSoft\CedeSafe"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\CedeSoft\CedeSafe" ""

;--------------------------------
;Variables

  Var MUI_TEMP
  Var STARTMENU_FOLDER

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "D:\Work\Programming\Win32Work\ScanUserWin\XPInstaller\CCLicense.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\CedeSoft\CedeSafe" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
  
  !insertmacro MUI_PAGE_STARTMENU Application $STARTMENU_FOLDER
  
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "CedeSafe" SecDummy

  SetOutPath "$INSTDIR"
  
  ;ADD YOUR OWN FILES HERE...
  File "D:\Work\Programming\Win32Work\ScanUserWin\XPInstaller\CedeSafe.exe"
  File "D:\Work\Programming\Win32Work\ScanUserWin\XPInstaller\CedeSafe.inf"
  File "D:\Work\Programming\Win32Work\ScanUserWin\XPInstaller\cedesafe.sys"
  File "D:\Work\Programming\Win32Work\ScanUserWin\XPInstaller\CloseCedeSafe.exe"
  
  ;Store installation folder
  WriteRegStr HKCU "Software\CedeSoft\CedeSafe" "" $INSTDIR
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    SetShellVarContext current
    
    ;Create shortcuts
    CreateDirectory "$SMPROGRAMS\$STARTMENU_FOLDER"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\Uninstall.lnk" "$INSTDIR\Uninstall.exe"
    CreateShortCut "$SMPROGRAMS\$STARTMENU_FOLDER\CedeSafe.lnk" "$INSTDIR\CedeSafe.exe"
    CreateShortCut "$SMSTARTUP\CedeSafe.lnk" "$INSTDIR\CedeSafe.exe"
  !insertmacro MUI_STARTMENU_WRITE_END

  ; Install the driver
  ExecWait 'RUNDLL32.EXE SETUPAPI.DLL,InstallHinfSection DefaultInstall 132 $INSTDIR\CedeSafe.inf'
  
  ; Start the driver
  ExecWait 'net start CedeSafe'

  ; Startup the engine
  Exec '"$INSTDIR\CedeSafe.exe"'

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecDummy ${LANG_ENGLISH} "CedeCrypt - Powerful encryption to secure your data."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecDummy} $(DESC_SecDummy)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...
  ; First shut down the engine if it is running
  ExecWait '"$INSTDIR\CloseCedeSafe.exe"'

  ; Uninstall the driver
 ExecWait 'RUNDLL32.EXE SETUPAPI.DLL,InstallHinfSection DefaultUninstall 132 $INSTDIR\CedeSafe.inf'

 ; Now remove the files and shortcuts
  Delete "$INSTDIR\Uninstall.exe"
  Delete /REBOOTOK "$INSTDIR\CedeSafe.exe"
  Delete /REBOOTOK "$INSTDIR\cedesafe.sys"
  Delete /REBOOTOK "$INSTDIR\CedeSafe.inf"
  Delete /REBOOTOK "$INSTDIR\CloseCedeSafe.exe"
  
  DeleteRegValue HKCU "Software\CedeSoft\CedeCrypt" "AutoStart"
  DeleteRegValue HKCU "Software\CedeSoft\CedeCrypt" "RteVerify"
  DeleteRegValue HKCU "Software\CedeSoft\CedeCrypt" "UseAlerts"
  DeleteRegValue HKCU "Software\CedeSoft\CedeCrypt" "UseIncludeFolders"

  
  RMDir /REBOOTOK "$INSTDIR"
  
  !insertmacro MUI_STARTMENU_GETFOLDER Application $MUI_TEMP
SetShellVarContext current


  Delete "$SMPROGRAMS\$MUI_TEMP\Uninstall.lnk"
  Delete "$SMPROGRAMS\$MUI_TEMP\CedeSafe.lnk"
  Delete "$SMSTARTUP\CedeSafe.lnk"
  
  ;Delete empty start menu parent diretories
  StrCpy $MUI_TEMP "$SMPROGRAMS\$MUI_TEMP"
 
  startMenuDeleteLoop:
	ClearErrors
    RMDir $MUI_TEMP
    GetFullPathName $MUI_TEMP "$MUI_TEMP\.."
    
    IfErrors startMenuDeleteLoopDone
  
    StrCmp $MUI_TEMP $SMPROGRAMS startMenuDeleteLoopDone startMenuDeleteLoop
  startMenuDeleteLoopDone:

  DeleteRegKey /ifempty HKCU "Software\CedeSoft\CedeSafe"

SectionEnd