// MyOverlayIcon.cpp : Implementation of 

#include "stdafx.h"
#include <stdio.h>
#include "MyOverlayIcon.h"


// CMyOverlayIcon

// IShellIconOverlayIdentifier Method Implementation 
// IShellIconOverlayIdentifier::GetOverlayInfo
// returns The Overlay Icon Location to the system
STDMETHODIMP CMyOverlayIcon::GetOverlayInfo(
  LPWSTR pwszIconFile,
  int cchMax,int* pIndex,
  DWORD* pdwFlags)
{
  GetModuleFileNameW(_AtlBaseModule.GetModuleInstance(), pwszIconFile, cchMax);

  *pIndex = 0;
  *pdwFlags = ISIOI_ICONFILE | ISIOI_ICONINDEX;

  return S_OK;
}

// IShellIconOverlayIdentifier Method Implementation 

// returns the priority of this overlay 0 being the highest. 
// this overlay is always selected do to its high priority 
STDMETHODIMP CMyOverlayIcon::GetPriority(int* pPriority)
{
  // highest priority
  *pPriority=0;
  return S_OK;
}

void CMyOverlayIcon::Test (char *szTest)
{

}

unsigned long CMyOverlayIcon::GetExFileSize (wchar_t *FileName)
{
	// Check if the file exists		
	struct _wfinddata_t c_file;
	long hFile;

	// Build the list of files in the source path
	if( (hFile = _wfindfirst(FileName, &c_file )) == -1L ) {
		return 0;
	} else {
		return c_file.size;
	}

	return 0;
}

bool CMyOverlayIcon::IsFileEncrypted (wchar_t *szFilename)
{
	unsigned long m_lMagicone = 14772895557;
	unsigned long m_lMagictwo = 74922457155;
	unsigned long m_lMagicthree = 55555777777;
	unsigned long m_lMagicfour = 77777774444;

	unsigned long lSigone = 0;
	unsigned long lSigtwo = 0;
	unsigned long lSigthree = 0;
	unsigned long lSigfour = 0;

	bool bSigmatch = true;

	// If the file is not large enough to hold the signature then return false;
	if (GetExFileSize (szFilename) < (sizeof (unsigned long) * 4)) {
		//OutputText ("File not large enough to be encrypted.");
		return false;
	}

	FILE *hSource;

	if (hSource = _wfopen (szFilename, L"rb")) {
		// Return nothing
	} else {
		//OutputText ("Unable to open file for reading!");
		return false;
	}

	fread (&lSigone, 1, sizeof (unsigned long), hSource);	
	fread (&lSigtwo, 1, sizeof (unsigned long), hSource);
	fread (&lSigthree, 1, sizeof (unsigned long), hSource);
	fread (&lSigfour, 1, sizeof (unsigned long), hSource);


	if (lSigone != m_lMagicone) {
		bSigmatch = false;
	}

	if (lSigtwo != m_lMagictwo) {
		bSigmatch = false;
	}

	if (lSigthree != m_lMagicthree) {
		bSigmatch = false;
	}

	if (lSigfour != m_lMagicfour) {
		bSigmatch = false;
	}


	if(hSource)
	{
		if(fclose(hSource)) {
			//OutputText ("Unable to close file!");			
		}			
	}

	if (bSigmatch == true) {
		return true;
	} else {
		return false;
	}
}

// IShellIconOverlayIdentifier Method Implementation
// IShellIconOverlayIdentifier::IsMemberOf
// Returns Whether the object should have this overlay or not 
STDMETHODIMP CMyOverlayIcon::IsMemberOf(LPCWSTR pwszPath, DWORD dwAttrib)
{
  //wchar_t *s = _wcsdup(pwszPath);
  HRESULT r = S_FALSE;
  //
  //_wcslwr(s);

  //// Criteria
  //if (wcsstr(s, L"codeproject") != 0)
  //  r = S_OK;

  //free(s);

  //return r;
	
	
  if (IsFileEncrypted ((wchar_t *) pwszPath) == true) {
	r = S_OK;
  }
	

	//if (WideCharToMultiByte (CP_ACP, WC_COMPOSITECHECK, pwszPath, -1, szPath, 1024, NULL, lpUsedDefault) != 0) {
		//r = S_OK;
	//}

	return r;
}
