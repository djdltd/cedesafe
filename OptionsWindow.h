#pragma once
#include <windows.h>
#include <io.h>
#include <commctrl.h>
#include <shlobj.h>
#include "UIWindow.h"
#include "CommonFunctions.h"
#include "UserSettings.h"


class OptionsWindow : public UIWindow
{
	public:
		OptionsWindow ();
		~OptionsWindow ();				
		void Initialise (HWND hWnd, unsigned int uID);
		void CreateGeneralPanel ();
		void CreateFileTypesPanel ();
		void CreateApplicationsPanel ();
		void CreateFoldersPanel ();
		bool BrowseForFolder (LPCTSTR szTitle, char *szOutPath);

		void SetFoldersPanelVisible (bool bVisible);
		void SetApplicationsPanelVisible (bool bVisible);
		void SetFileTypesPanelVisible (bool bVisible);
		void SetGeneralPanelVisible (bool bVisible);
		void ActivatePanel (char *szPanelname);
		void RefreshIncludeFolders ();
		void RefreshIndividualSettings ();

		void SetDefaultDirs (DynList *pdlexdir);
		void SetConfig (DynList *pdlextensions, DynList *pdlapps, DynList *pdlexdir, DynList *pdlindir, UserSettings *psettings);
		void GetConfig (DynList *pdlextensions, DynList *pdlapps, DynList *pdlexdir, DynList *pdlindir, UserSettings *psettings);
	private:
		// Private Member Variables & objects				
		
		// ID ofthis window - required for window class registration purposes
		unsigned int m_ID;

		// Global Window Handle
		HWND m_hwnd;
		HWND m_parenthwnd;
		HBITMAP hbmBanner;
		
		bool m_bInitialised;

		// Common functions library
		CommonFunctions m_cmf;

		// Default filtered dirs
		DynList *m_pdldefault;

		// Control handles
		HWND m_lstoptpanel;
		HWND m_btnclose;

		// General Panel
		HWND m_grpgeneral;
		HWND m_chkstartup;
		HWND m_chkalerts;

		// FileTypes Panel
		HWND m_grpfiletypes;
		HWND m_lblftstatic;
		HWND m_lstfiletypes;
		HWND m_txtnewext;
		HWND m_btnadd;
		HWND m_btnremove;

		// Applications panel
		HWND m_grpapplications;
		HWND m_lblappstatic;
		HWND m_lstapplications;
		HWND m_txtnewapp;
		HWND m_btnaddapp;
		HWND m_btnremoveapp;

		// Folders Panel
		HWND m_grpfolders;
		HWND m_lblfolderexclusionsstatic;
		HWND m_lstexcludedfolders;
		HWND m_btnaddexclusion;
		HWND m_btnremoveexclusion;
		HWND m_chkincludeonly;
		HWND m_lblfolderinclusionstatic;
		HWND m_lstincludedfolders;
		HWND m_btnaddinclusion;
		HWND m_btnremoveinclusion;

		// Individual settings
		bool m_bincludefoldersonly;
		bool m_bautostart;
		bool m_busealerts;

		// Flag indicating if we're using diagnostics
		bool m_bUseDiagnostics;

		// Registered class name
		// We need a different class name for every instance of
		// this window. This class name
		// Is created by the Initialise routine
		// with a uID value suffixed to the end
		char m_szClassname[SIZE_STRING];

		// event notification from base class
		void OnDestroy (HWND hWnd);
		void OnCreate (HWND hWnd);		
		void OnCommand (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnUICommand (HWND hWnd, WPARAM wParam, LPARAM lParam);		
		void OnUIScroll (HWND hWnd, WPARAM wParam, LPARAM lParam);
		void OnPaint (HWND hWnd);
		void OnTimer (WPARAM wParam);
		void OnMouseMove (HWND hWnd, int mouseXPos, int mouseYPos);
		void OnLButtonDown (HWND hWnd);
		void OnLButtonUp (HWND hWnd);
};
