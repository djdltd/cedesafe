#pragma once
#include <windows.h>
#include "RegistryHandler.h"
#include "resource.h"

class UserSettings {
	
	public:
		UserSettings ();
		~UserSettings ();
		
		bool bUseIncludeFolders;
		bool bAutoStart;
		bool bUseAlerts;

		void Save ();
		void Load ();
	private:
		
		RegistryHandler m_reg;

};
