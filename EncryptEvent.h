#pragma once
#include <windows.h>

class EncryptEvent {
	
	#define SIZE_STRING		1024
	#define SIZE_INTEGER	32
	#define SIZE_NAME			64

	public:
		EncryptEvent ();
		~EncryptEvent ();
		
		unsigned int iEventID;
		char szFilepath[SIZE_STRING];
		HANDLE hEventThread;
		DWORD dwEventThreadID;
		bool bInuse;
		bool cipherinprogress;
		unsigned int iTimer;
		bool bCancel;
	private:
		


};
