
#include <windows.h>
#include "resource.h"
#include <psapi.h>
#include <stdlib.h>
#include <stdio.h>
#include <winioctl.h>
#include <string.h>
#include <crtdbg.h>
#include <assert.h>
#include <fltuser.h>
#include "MemoryBuffer.h"
#include "StandardEncryption.h"
#include "scanuk.h"
#include "scanuser.h"
#include "DynList.h"
#include "EncryptEvent.h"
#include "StringItem.h"
#include "PasswordWindow.h"
#include "RegistryHandler.h"
#include "OptionsWindow.h"
#include "CommonFunctions.h"
#include "UserSettings.h"
#include <dontuse.h>


/////////////// GLOBALS ///////////////

HWND m_hwnddiaglist;
HWND m_btntestbutton;
HWND m_btntestbutton2;
HWND m_btnshowalert;
HWND m_btnlistevents;

// Global window handle
HWND g_hwnd;

// Global unique message for simple IPC (Interprocess communication)
// basically used for shutting us down in the event of an uninstall
UINT g_ipcmessage;

// Standard encryption class
StandardEncryption g_enc;

bool m_bUsingdiagnostics = true;
unsigned int m_bMaxwritelen = 0;
DynList m_dlencryptqueue;

EncryptEvent m_fixedcryptevents[MAXFIXEDEVENTS];
unsigned int m_inextcrypteventpointer = 0;

unsigned int m_iavaileventid = 0;
bool m_beventgenbusy = false;
unsigned int m_iwaitamount = 1;

FARPROC PasswordWindow::lpfnOldWndProc = NULL;
FARPROC PasswordWindow::lpfnOldWndProc2 = NULL;
PasswordWindow *PasswordWindow::ppwnd = NULL;

// The DynList containing all of the file extensions we're interested in
DynList m_dlwantedextensions;

// The DynList containing all of the filtered directories. The directories
// that should bypass the encryption filter like Program Files and Windows
DynList m_dlfiltereddirectories;
DynList m_dldefaultfiltereddirs;

// The DynList containing all of the filtereed processes. The processes
// That should bypass the encryption filter like explorer.exe and rtvscan.exe
DynList m_dlfilteredapps;

// DynList containing all of the included directories only. If the user has
// chosen to specify inclusions only then only these folders will be subject
// to realtime encryption. Exclusions will be bypassed.
DynList m_dlincludeddirectories;

// User customisable settings
UserSettings m_settings;

// System Tray icon vars
NOTIFYICONDATA m_nid;
HICON m_hSystrayicon;
HICON m_hSystrayiconleft;
HICON m_hSystrayiconright;
HICON m_hSystrayicontop;
HICON m_hSystrayiconbottom;
HICON m_hSystrayiconerror;
unsigned int m_ianimframe = 0;
bool m_bstopanimtimeractive = false;
bool m_banimtimeractive = false;

// The system tray popup menu
HMENU m_hTrayMenu;

// The password input window
PasswordWindow m_passwindow;

// Realtime Engine enabled
bool g_bEngineenabled = true;

// The stored session password
char g_sessionpassword[SIZE_STRING];
bool g_bsessionpasswordok = false;

// Realtime Alert Window (for notification messages)
HWND g_hwndAlertPopup = NULL;
HBITMAP g_hbmAlertbitmap = NULL;
bool g_bAlertdialogvisible = false;
int g_ianimxpos = 0;
bool g_bAlertdialogcreated = false;
HWND g_hwndlblPopup = NULL;
char szAlertmessage[SIZE_STRING];

// Options window
OptionsWindow m_options;

// The Registry Handler class
RegistryHandler m_reg;

// About Window
HWND g_hwndAboutDialog = NULL;
HBITMAP g_hbmAboutBanner = NULL;

// Encryption log Dialog
HWND g_hwndEnclogDialog = NULL;
HWND m_hwndencloglist = NULL;

// Common Functions
CommonFunctions m_cmf;

// Screen Resolution
unsigned int g_iScreenResX = 0;
unsigned int g_iScreenResY = 0;

//
//  Default and Maximum number of threads.
//

#define SCANNER_DEFAULT_REQUEST_COUNT       15
#define SCANNER_DEFAULT_THREAD_COUNT        32
#define SCANNER_MAX_THREAD_COUNT            64

UCHAR g_WantedExtension[] = ".txt";

//
//  Context passed to worker threads
//

typedef struct _SCANNER_THREAD_CONTEXT {

    HANDLE Port;
    HANDLE Completion;
} SCANNER_THREAD_CONTEXT, *PSCANNER_THREAD_CONTEXT;

////////////////////////////////////////



/////////////// PROTOTYPES ///////////////
void CreateEventThread (int eventID);
bool Findstring (char *szStringtosearch, char *szSearchstring);
void AlertAnimHide ();
void AlertAnimShow ();
void SetAlertText (char *szText);
void SetAlertText (char *szProcess, char *szText);
void StartFullTrayAnim ();
void StopFullTrayAnim ();

////////////////////////////////////////////


/////////////// IMPLEMENTATION ///////////////

int GetNewCryptEventID ()
{
	unsigned int iRes = 0;

	int i = 0;

	EncryptEvent curEvent;

	for (i=0;i<MAXFIXEDEVENTS;i++) {
		curEvent = m_fixedcryptevents[i];

		if (curEvent.bInuse == false) {			
			m_fixedcryptevents[i] = curEvent;
			return i;
		}
	}

	// If we got here then there was no free event
	return -1;
}

bool ResetEncryptEvent (char *szFilepath)
{
	// This goes through all of the fixed encrypt events and if one exists with the file
	// name supplied, then the encryption timer is reset back to zero. This is in the event
	// we receive multiple CLOSE events for the same file.

	int i = 0;
	EncryptEvent curEvent;

	for (i=0;i<MAXFIXEDEVENTS;i++) {
		curEvent = m_fixedcryptevents[i];

		if (curEvent.bInuse == true) {
			if (strcmp (szFilepath, curEvent.szFilepath) == 0) {
				curEvent.iTimer = 0;
				curEvent.bCancel = false;
				m_fixedcryptevents[i] = curEvent;
				return true;
			}
		}
	}

	return false;
}

void CancelEncryptEvent (char *szFilepath)
{
	// This is called by a create on a file, we have to first check that we are
	// not about to encrypt a file, if an encryption process is about to begin
	// we cancel it

	int i = 0;

	EncryptEvent curEvent;

	for (i=0;i<MAXFIXEDEVENTS;i++) {
		curEvent = m_fixedcryptevents[i];

		if (curEvent.bInuse == true) {
			if (strcmp (szFilepath, curEvent.szFilepath) == 0) {
				curEvent.bCancel = true;
				m_fixedcryptevents[i] = curEvent;
			}
		}
	}
}

void OutputInt (LPCSTR lpszText, int iValue)
{
	if (m_bUsingdiagnostics == false) {
		return;
	}
	
	char szInteger[SIZE_INTEGER];
	ZeroMemory (szInteger, SIZE_INTEGER);
	sprintf_s (szInteger, SIZE_INTEGER, "%d", iValue);

	char szText[SIZE_STRING];
	ZeroMemory (szText, SIZE_STRING);


	sprintf_s (szText, SIZE_STRING, "%d: ", GetTickCount ());

	strcat_s (szText, SIZE_STRING, lpszText);

	strcat_s (szText, SIZE_STRING, szInteger);

	SendMessage (m_hwnddiaglist, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) &szText);

	int lCount = SendMessage (m_hwnddiaglist, LB_GETCOUNT, 0, 0);
	SendMessage (m_hwnddiaglist, LB_SETCURSEL, lCount-1, 0);
}


void OutputInt (LPCSTR lpszObjText, LPCSTR lpszText, int iValue)
{
	if (m_bUsingdiagnostics == false) {
		return;
	}

	char szInteger[SIZE_INTEGER];
	ZeroMemory (szInteger, SIZE_INTEGER);
	sprintf_s (szInteger, SIZE_INTEGER, "%d", iValue);

	char szText[SIZE_STRING];
	ZeroMemory (szText, SIZE_STRING);


	sprintf_s (szText, SIZE_STRING, "%d: ", GetTickCount ());

	strcat_s (szText, SIZE_STRING, lpszObjText);

	strcat_s (szText, SIZE_STRING, lpszText);

	strcat_s (szText, SIZE_STRING, szInteger);

	SendMessage (m_hwnddiaglist, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) &szText);

	int lCount = SendMessage (m_hwnddiaglist, LB_GETCOUNT, 0, 0);
	SendMessage (m_hwnddiaglist, LB_SETCURSEL, lCount-1, 0);
}


void OutputText (LPCSTR lpszText)
{
	if (m_bUsingdiagnostics == false) {
		return;
	}

	char szText[SIZE_STRING];
	ZeroMemory (szText, SIZE_STRING);

	sprintf_s (szText, SIZE_STRING, "%d: ", GetTickCount ());

	strcat_s (szText, SIZE_STRING, lpszText);
	SendMessage (m_hwnddiaglist, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) &szText);

	int lCount = SendMessage (m_hwnddiaglist, LB_GETCOUNT, 0, 0);
	SendMessage (m_hwnddiaglist, LB_SETCURSEL, lCount-1, 0);
}

void OutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUsingdiagnostics == false) {
		return;
	}

	char szText[SIZE_STRING*2];
	ZeroMemory (szText, SIZE_STRING*2);	

	sprintf_s (szText, SIZE_STRING, "%d: ", GetTickCount ());

	strcat_s (szText, SIZE_STRING*2, lpszName);
	strcat_s (szText, SIZE_STRING*2, lpszValue);

	SendMessage (m_hwnddiaglist, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) &szText);

	int lCount = SendMessage (m_hwnddiaglist, LB_GETCOUNT, 0, 0);
	SendMessage (m_hwnddiaglist, LB_SETCURSEL, lCount-1, 0);
}

void OutputText (LPCSTR lpszObjText, LPCSTR lpszName, LPCSTR lpszValue)
{
	if (m_bUsingdiagnostics == false) {
		return;
	}

	char szText[SIZE_STRING*2];
	ZeroMemory (szText, SIZE_STRING*2);	

	sprintf_s (szText, SIZE_STRING, "%d: ", GetTickCount ());

	strcat_s (szText, SIZE_STRING*2, lpszObjText);
	strcat_s (szText, SIZE_STRING*2, lpszName);
	strcat_s (szText, SIZE_STRING*2, lpszValue);

	SendMessage (m_hwnddiaglist, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) &szText);

	int lCount = SendMessage (m_hwnddiaglist, LB_GETCOUNT, 0, 0);
	SendMessage (m_hwnddiaglist, LB_SETCURSEL, lCount-1, 0);
}

void LogOutputText (LPCSTR lpszName, LPCSTR lpszValue)
{
	char szText[SIZE_STRING*2];
	ZeroMemory (szText, SIZE_STRING*2);	

	SYSTEMTIME systime;

	GetLocalTime (&systime);

	sprintf_s (szText, SIZE_STRING, "%i/%i/%i %i:%i:%i - ", systime.wDay, systime.wMonth, systime.wYear, systime.wHour, systime.wMinute, systime.wSecond);

	strcat_s (szText, SIZE_STRING*2, lpszName);
	strcat_s (szText, SIZE_STRING*2, lpszValue);

	SendMessage (m_hwndencloglist, LB_ADDSTRING, 0, (LPARAM) (LPCTSTR) &szText);

	int lCount = SendMessage (m_hwndencloglist, LB_GETCOUNT, 0, 0);
	SendMessage (m_hwndencloglist, LB_SETCURSEL, lCount-1, 0);
}


bool IsExtensionMatched (char *szFilename, char *szExt)
{
        int c = 0;

        char szExtension[SIZE_NAME];
        ZeroMemory (szExtension, SIZE_NAME);

        char szTemp[SIZE_NAME];
        ZeroMemory (szTemp, SIZE_NAME);

        strcpy_s (szTemp, SIZE_NAME, szExt);

        strncpy (szExtension, szFilename+(strlen(szFilename)-4), 4);

        _strupr (szTemp);
        _strupr (szExtension);

        if (strcmp (szTemp, szExtension) == 0) {
                return true;
        } else {
                return false;
        }
}

bool IsPathValid (char *szPath)
{
	// Basically a function to check that the supplied path does not
	// contains any illegal characters such as * for wildcards, we don't want those
	return true;

	char szCurchar[SIZE_NAME];
	int c = 0;
	bool IsValid = true;

	for (c=0;c<strlen (szPath);c++) {
		ZeroMemory (szCurchar, SIZE_NAME);
		strncpy (szCurchar+c, szPath, 1);

		if (strcmp (szCurchar, "*") == 0) {
			IsValid = false;
		}
	}
	
	return IsValid;
}


void GetProcessName (char *szOutname, ULONG processID)
{
    TCHAR szProcessName[MAX_PATH] = TEXT("<unknown>");
	bool bResult = false;

    // Get a handle to the process.
    HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |
                                   PROCESS_VM_READ,
                                   FALSE, processID );
	if (NULL != hProcess )
    {
        HMODULE hMod;
        DWORD cbNeeded;

        if ( EnumProcessModules( hProcess, &hMod, sizeof(hMod), 
             &cbNeeded) )
        {
            GetModuleBaseName( hProcess, hMod, szProcessName, 
                               sizeof(szProcessName)/sizeof(TCHAR) );
        }
    }

	_strupr (szProcessName);
	ZeroMemory (szOutname, SIZE_STRING);
	strcpy_s (szOutname, SIZE_STRING, szProcessName);
}

bool IsProcessValid (ULONG processID, char *szOutprocess) {

	char szProcess[SIZE_STRING];
	ZeroMemory (szProcess, SIZE_STRING);
	GetProcessName (szProcess, processID);	
	
	bool bProcessvalid = true;

	int i = 0;

	_strupr (szProcess);
	
	char szCurrent[SIZE_STRING];
	StringItem *pitem;

	for (i=0;i<m_dlfilteredapps.GetNumItems ();i++) {
		ZeroMemory (szCurrent, SIZE_STRING);
		pitem = (StringItem *) m_dlfilteredapps.GetItem (i);
		strcpy_s (szCurrent, SIZE_STRING, pitem->szItem);		
		_strupr (szCurrent);

		if (strcmp (szProcess, szCurrent) == 0) {
			bProcessvalid = false;
		}
	}

	ZeroMemory (szOutprocess, SIZE_STRING);
	strcpy_s (szOutprocess, SIZE_STRING, szProcess);

	return bProcessvalid;
}

void AddWantedExtension (char *szExtension)
{
	// Add a single extension to the global dynlist using
	// StringItem objects
	StringItem item;
	ZeroMemory (item.szItem, SIZE_STRING);
	strcpy_s (item.szItem, SIZE_STRING, szExtension);

	m_dlwantedextensions.AddItem (&item, sizeof (StringItem), false);
}

void AddFilteredApp (char *szAppname)
{
	StringItem item;
	ZeroMemory (item.szItem, SIZE_STRING);
	strcpy_s (item.szItem, SIZE_STRING, szAppname);

	m_dlfilteredapps.AddItem (&item, sizeof (StringItem), false);
}

void AddEnvFilteredDir (char *szEnvname)
{
	// Adds a filtered directory to our filtered directories dynlist from an environment
	// variable. The equivalent DOS short name is also added to the filter.

	char szEnvvalue[SIZE_STRING];
	ZeroMemory (szEnvvalue, SIZE_STRING);
	
	StringItem longpath;
	StringItem shortpath;

	if (GetEnvironmentVariable (szEnvname, szEnvvalue, SIZE_STRING) != 0) {
		// Success on the environment variable
		
		ZeroMemory (longpath.szItem, SIZE_STRING);
		strcpy_s (longpath.szItem, SIZE_STRING, szEnvvalue);
		m_dlfiltereddirectories.AddItem (&longpath, sizeof (StringItem), false);
		m_dldefaultfiltereddirs.AddItem (&longpath, sizeof (StringItem), false);

		ZeroMemory (shortpath.szItem, SIZE_STRING);

		if (GetShortPathName (longpath.szItem, shortpath.szItem, SIZE_STRING) != 0) {
			// Success on get short path

			_strupr (longpath.szItem);
			_strupr (shortpath.szItem);

			// Now check that the long path and short path are different
			if (strcmp (longpath.szItem, shortpath.szItem) != 0) {
				m_dlfiltereddirectories.AddItem (&shortpath, sizeof (StringItem), false);
				m_dldefaultfiltereddirs.AddItem (&shortpath, sizeof (StringItem), false);
			}
		}
	}
}

void AddEnvFilteredDir (char *szEnvname, char *Subdir)
{
	// Adds a filtered directory to our filtered directories dynlist from an environment
	// variable. The equivalent DOS short name is also added to the filter. This function
	// Also appends the Subdir parameter to the path retrieved by the environment variable

	char szEnvvalue[SIZE_STRING];
	ZeroMemory (szEnvvalue, SIZE_STRING);
	
	StringItem longpath;
	StringItem shortpath;

	if (GetEnvironmentVariable (szEnvname, szEnvvalue, SIZE_STRING) != 0) {
		// Success on the environment variable
		
		ZeroMemory (longpath.szItem, SIZE_STRING);
		strcpy_s (longpath.szItem, SIZE_STRING, szEnvvalue);
		strcat_s (longpath.szItem, SIZE_STRING, Subdir);

		m_dlfiltereddirectories.AddItem (&longpath, sizeof (StringItem), false);
		m_dldefaultfiltereddirs.AddItem (&longpath, sizeof (StringItem), false);

		ZeroMemory (shortpath.szItem, SIZE_STRING);

		if (GetShortPathName (longpath.szItem, shortpath.szItem, SIZE_STRING) != 0) {
			// Success on get short path

			_strupr (longpath.szItem);
			_strupr (shortpath.szItem);

			// Now check that the long path and short path are different
			if (strcmp (longpath.szItem, shortpath.szItem) != 0) {
				m_dlfiltereddirectories.AddItem (&shortpath, sizeof (StringItem), false);
				m_dldefaultfiltereddirs.AddItem (&shortpath, sizeof (StringItem), false);
			}
		}
	}
}


bool DoesEnvExist (char *szEnvname)
{
	// Quick function to check if an environment variable exists
	char szEnvvalue[SIZE_STRING];
	ZeroMemory (szEnvvalue, SIZE_STRING);

	if (GetEnvironmentVariable (szEnvname, szEnvvalue, SIZE_STRING) != 0) {
		return true;
	} else {
		return false;
	}
}

void PrintFilteredDirectories ()
{
	int d = 0;
	StringItem *pcuritem;

	for (d=0;d<m_dlfiltereddirectories.GetNumItems ();d++) {
		pcuritem = (StringItem *) m_dlfiltereddirectories.GetItem (d);

		OutputText (pcuritem->szItem);
	}
}

void SetupFilteredDirectories ()
{
	if (DoesEnvExist ("LocalAppData") == true) {
		AddEnvFilteredDir ("LocalAppData");
	} else {
		AddEnvFilteredDir ("UserProfile", "\\Local Settings");
	}

	AddEnvFilteredDir ("SystemRoot");
	AddEnvFilteredDir ("ProgramFiles");
	AddEnvFilteredDir ("AppData");
}

bool IsDirectoryFiltered (char *szFullpath)
{
	int d = 0;
	StringItem *pcuritem;

	for (d=0;d<m_dlfiltereddirectories.GetNumItems ();d++) {
		pcuritem = (StringItem *) m_dlfiltereddirectories.GetItem (d);

		if (Findstring (szFullpath, pcuritem->szItem) == true) {
			return true;
		}
	}

	return false;
}

bool IsDirectoryIncluded (char *szFullpath)
{
	int d = 0;
	StringItem *pcuritem;

	for (d=0;d<m_dlincludeddirectories.GetNumItems ();d++) {
		pcuritem = (StringItem *) m_dlincludeddirectories.GetItem (d);

		if (Findstring (szFullpath, pcuritem->szItem) == true) {
			return true;
		}
	}

	return false;
}

void SetupWantedExtensions ()
{
	// Add the wanted extensions to the global dynlist
	AddWantedExtension (".txt");
	AddWantedExtension (".bmp");
	AddWantedExtension (".doc");
	AddWantedExtension (".xls");
	AddWantedExtension (".ppt");
	AddWantedExtension (".rtf");
}

void SetupFilteredApps ()
{
	// Add the default filtered apps to the global dynlist
	AddFilteredApp ("EXPLORER.EXE");
	AddFilteredApp ("SVCHOST.EXE");
	AddFilteredApp ("VMWARESERVICE.EXE");
	AddFilteredApp ("VMWAREUSER.EXE");
	AddFilteredApp ("VMWARETRAY.EXE");
	AddFilteredApp ("<UNKNOWN>");
	AddFilteredApp ("RTVSCAN.EXE");
}

bool IsWantedExtension (char *szPath)
{
	StringItem *pitem;
	bool bwantedextension = false;
	int s = 0;

	for (s=0;s<m_dlwantedextensions.GetNumItems ();s++) {
		pitem = (StringItem *) m_dlwantedextensions.GetItem (s);

		if (IsExtensionMatched (szPath, pitem->szItem) == true) {
			bwantedextension = true;
		}
	}

	return bwantedextension;
}

bool IsCipherinprogress (char *szPath)
{
	int i = 0;

	EncryptEvent curEvent;

	for (i=0;i<MAXFIXEDEVENTS;i++) {
		curEvent = m_fixedcryptevents[i];

		if (curEvent.bInuse == true) {
			if (strcmp (szPath, curEvent.szFilepath) == 0) {
				if (curEvent.cipherinprogress == true) {
					return true;
				} else {
					return false;
				}				
			}
		}
	}
	return false;
}

bool ContainsChar (char *szSource, char *szChar) {
	
	int c = 0;
	char szCurchar[SIZE_NAME];
		
	// First the szSource has to be greater than zero
	if (strlen (szSource) > 0) {
		
		for (c=0;c<strlen (szSource);c++) {
			ZeroMemory (szCurchar, SIZE_NAME);		
			strncpy_s (szCurchar, SIZE_NAME, szSource+c, 1);

			if (strcmp (szCurchar, szChar) == 0) {
				return true;
			}
		}
	}

	return false;
}


bool FileExists (char *szFilepath)
{
	struct _finddata_t c_file;
	long hFile;

	if( (hFile = _findfirst(szFilepath, &c_file )) == -1L ) {
		return false;
	}
	else
	{
		return true;
	}
}

unsigned long FileSize (char *szFilepath)
{
	struct _finddata_t c_file;
	long hFile;

	if( (hFile = _findfirst(szFilepath, &c_file )) == -1L ) {
		return 0;
	}
	else
	{
		return c_file.size;
	}
}


bool Findstring (char *szStringtosearch, char *szSearchstring)
{
	char szFull[SIZE_STRING];
	char szSearch[SIZE_STRING];
	char szTemp[SIZE_STRING];
	int c = 0;

	if (strlen (szStringtosearch) > 0) {	
		if (strlen (szStringtosearch) >= strlen (szSearchstring)) {
			ZeroMemory (szFull, SIZE_STRING);
			strcpy_s (szFull, SIZE_STRING, szStringtosearch);

			ZeroMemory (szSearch, SIZE_STRING);
			strcpy_s (szSearch, SIZE_STRING, szSearchstring);

			_strupr (szFull);
			_strupr (szSearch);

			for (c=0;c<=(strlen (szFull)-strlen (szSearch));c++) {
				
				ZeroMemory (szTemp, SIZE_STRING);
				strncpy_s (szTemp, SIZE_STRING, szFull+c, strlen (szSearch));

				if (strcmp (szTemp, szSearch) == 0) {
					return true;	
				}
			}			
		} else {
			return false;
		}
	} else {
		return false;
	}

	return false;
}

BOOL
ScanBuffer (
    __in_bcount(BufferSize) PUCHAR Buffer,
    __in ULONG BufferSize, int NotifyType, PVOID origBuf, ULONG writeLen, ULONG processID, PUCHAR VolName
    )
{
	char strTest[10];
    PUCHAR p;
	char ctest[1024];
	char szCompletedpath[SIZE_STRING];
	ULONG searchStringLength = sizeof(g_WantedExtension) - sizeof(UCHAR);
	char szMsg[SIZE_STRING];
	MemoryBuffer memBuffer;
	EncryptEvent cryptEvent;
	EncryptEvent checkEvent;
	char szContents[SIZE_STRING];
	char szProcess[SIZE_STRING];
	char szVolName[SIZE_STRING];
	bool bOktocontinue = true;

	int inewevent = 0;


	strcpy_s (ctest, 1024, (char *) Buffer);

	ZeroMemory (szVolName, SIZE_STRING);
	strcpy_s (szVolName, SIZE_STRING, (char *) VolName);

	if (writeLen > m_bMaxwritelen) {
		m_bMaxwritelen = writeLen;
	}

	if (strlen (ctest) <= 4) { // The file name needs to be at least 4 characters long for checking the extension
		bOktocontinue = false;
	}

	if (IsWantedExtension (ctest) == false) { // Check if the extension of the file is one we're interested in.
		bOktocontinue = false;
	}

	if (IsPathValid (ctest) == false) { // Check if this supplied path is valid
		bOktocontinue = false;
	}

	if (IsProcessValid (processID, szProcess) == false) { // Ensure the originating process is not on the exclusions list
		bOktocontinue = false;
	}

	if (bOktocontinue == true) {
		// Check and construct the full path of the file.
		// We're only interested in directly mounted disks, not network volumes.

		// First we have to ensure we have a volume
		if (strlen (szVolName) == 2) { // The volume name must contain 2 characters e.g. C: D: or E: etc...
			
			// Now check that the path supplied does not contain a colon, it must not be a mapped drive
			if (ContainsChar (ctest, ":") == false) {
				
				// If we got here, then the path is ok to be combined.
				ZeroMemory (szCompletedpath, SIZE_STRING);
				strcpy_s (szCompletedpath, SIZE_STRING, szVolName);
				strcat_s (szCompletedpath, SIZE_STRING, ctest);
				
				OutputText ("Constructed path: ", szCompletedpath);

			} else {
				bOktocontinue = false;
				OutputText ("Path not constructed - ctest contains colon: ", ctest);
			}

		} else {
			bOktocontinue = false;
			OutputText ("Path not constructed - szVolName is not 2!");
		}
	}

	if (bOktocontinue == true) {

		if (m_settings.bUseIncludeFolders == true) {
			if (IsDirectoryIncluded (szCompletedpath) == false) { // Check if this directory is an included directory
				bOktocontinue = false;
			}
		} else {
			if (IsDirectoryFiltered (szCompletedpath) == true) { // Check if this request comes from a directory we want to bypass
				bOktocontinue = false;
			}
		}		
	}

	if (bOktocontinue == true) { // If we're good to go, then proceed with processing the request
	
		OutputText ("AnalyseRequest: Originating Process: ", szProcess);
		OutputText ("AnalyseRequest: Volume: ", szVolName);
		OutputInt ("AnalyseRequest: Vol Length: ", strlen (szVolName));

		// Ensure that we don't do any thing with zero sized files.
		if (FileExists (szCompletedpath) == true) {
			if (FileSize (szCompletedpath) == 0) {
				return TRUE;
			}
		}

		if (NotifyType == 2) {
			
			ZeroMemory (szMsg, SIZE_STRING);
			sprintf_s (szMsg, SIZE_STRING, "CREATE: %s", szCompletedpath);
			OutputText (szMsg);

			// Cancel any encryption process, if any
			CancelEncryptEvent (szCompletedpath);

			// If the encryption is already in progress then we must wait for it to
			// finish
			while (IsCipherinprogress (szCompletedpath) == true) {
				OutputText ("AnalyseRequest: WAITING FOR CIPHER COMPLETION...");
				Sleep (1000);
			}

			// Check and decrypt file
			unsigned int encRes = g_enc.IsFileEncrypted (szCompletedpath);

			if (encRes == CRYPTRES_FAILED) {
				OutputText ("CREATE: Crypt Res failed!");
			} 

			if (encRes == CRYPTRES_ENCRYPTED) {
				OutputText ("CREATE: File IS encrypted");
				
				//char g_sessionpassword[SIZE_STRING];
				//bool g_bsessionpasswordok = false;
				
				if (g_bsessionpasswordok == true) {
					// Now decrypt the file.
					//PostMessage (g_hwnd, WM_STARTTRAYANIM, 0, 0);

					if (g_bEngineenabled == true) {
						if (g_enc.EncryptFileEx2 (szCompletedpath, szCompletedpath, g_sessionpassword, false) == true) {
							OutputText ("File decrypted successfully.");
							LogOutputText ("DECRYPTED FILE: ", szCompletedpath);
						} else {
							OutputText ("Unable to decrypt file. Sorry.");
							LogOutputText ("DECRYPTION FAILED: ", szCompletedpath);
						}
					} else {
						SetAlertText (szProcess, "will not be able to read this file because it is Encrypted, and Realtime Decryption has been disabled.");
						PostMessage (g_hwnd, WM_ACTIVATEALERT, 0, 0);
					}
					//PostMessage (g_hwnd, WM_STOPTRAYANIM, 0, 0);
				} else {

					
					SetAlertText (szProcess, "will not be able to read this file because it is Encrypted, and a password has not been specified.");
					PostMessage (g_hwnd, WM_ACTIVATEALERT, 0, 0);
				}



			}

			if (encRes == CRYPTRES_NOTENCRYPTED) {
				OutputText ("CREATE: File NOT encrypted - no decrypt necessary");
			}

			// If the file is not encrypted we don't do anything with it.

		}

		if (NotifyType == 3) {

			ZeroMemory (szMsg, SIZE_STRING);
			sprintf_s (szMsg, SIZE_STRING, "CLOSE: %s", szCompletedpath);
			OutputText (szMsg);

			// Encryption time, but only if we need to
			if (ResetEncryptEvent (szCompletedpath) == false) {
				// Only encrypt if there is no existing encryption event already taking place
				OutputText ("Reset encrypt event found no existing event. Spawning new encrypt thread...");

				// Post a message to the main window asking it to generate a new event id
				// But only do this if it's not busy, if it is busy then we wait
				
				while (m_beventgenbusy == true) {
					Sleep (m_iwaitamount);
				}

				PostMessage (g_hwnd, WM_GENEVENTID, 0, 0);

				// Now we wait until one was available
				while (m_beventgenbusy == true) {
					Sleep (m_iwaitamount);
				}

				inewevent = m_iavaileventid; // Get the next available static crypt event id we can use
				OutputInt ("ScanBuffer: GetNewCryptEventID returned: ", inewevent);

				if (inewevent != -1) {

					// If there was one available
					cryptEvent = m_fixedcryptevents[inewevent];

					// Copy the file path into the encryption event
					strcpy_s (cryptEvent.szFilepath, SIZE_STRING, szCompletedpath);

					// Make sure this event is flagged as in use
					cryptEvent.bInuse = true;
					cryptEvent.bCancel = false;

					// now put the encryption event back into the static array
					m_fixedcryptevents[inewevent] = cryptEvent;

					// Now we have to spawn a new encryption thread which takes in the id
					// if the encryption event and then begins encrypting the file.
					CreateEventThread (inewevent);


				} else {
					// This is where we have to place the file to be encrypted on a queue
				}

			} else {
				OutputText ("Reset encrypt event found! - Resetting timer for file.");
			}
		}
	} else {
		return TRUE;
	}

	return TRUE;
}

DWORD
ScannerWorker(
    __in PSCANNER_THREAD_CONTEXT Context
    )
/*++

Routine Description

    This is a worker thread that


Arguments

    Context  - This thread context has a pointer to the port handle we use to send/receive messages,
                and a completion port handle that was already associated with the comm. port by the caller

Return Value

    HRESULT indicating the status of thread exit.

--*/
{
    PSCANNER_NOTIFICATION notification;
    SCANNER_REPLY_MESSAGE replyMessage;
    PSCANNER_MESSAGE message;
    LPOVERLAPPED pOvlp;
    BOOL result;
    DWORD outSize;
    HRESULT hr;
    ULONG_PTR key;
	char szMsg[SIZE_STRING];
	


    while (TRUE) {

        //
        //  Poll for messages from the filter component to scan.
        //

        result = GetQueuedCompletionStatus( Context->Completion, &outSize, &key, &pOvlp, INFINITE );

        //
        //  Obtain the message: note that the message we sent down via FltGetMessage() may NOT be
        //  the one dequeued off the completion queue: this is solely because there are multiple
        //  threads per single port handle. Any of the FilterGetMessage() issued messages can be
        //  completed in random order - and we will just dequeue a random one.
        //

        message = CONTAINING_RECORD( pOvlp, SCANNER_MESSAGE, Ovlp );

        if (!result) {

            //
            //  An error occured.
            //

            hr = HRESULT_FROM_WIN32( GetLastError() );
            break;
        }

		//ZeroMemory (szMsg, SIZE_STRING);
        //sprintf_s (szMsg, SIZE_STRING,  "Received message, size %d", pOvlp->InternalHigh );
		//OutputText (szMsg);

        notification = &message->Notification;

       // assert(notification->BytesToScan <= SCANNER_READ_BUFFER_SIZE);
       // __analysis_assume(notification->BytesToScan <= SCANNER_READ_BUFFER_SIZE);
		//OutputInt ("SCANNERWORKER: ProcessID: ", notification->processID);
		result = ScanBuffer( notification->Contents, notification->BytesToScan, notification->NotifyType, notification->origBuf, notification->origBufSize, notification->processID, notification->Volname);

        replyMessage.ReplyHeader.Status = 0;
        replyMessage.ReplyHeader.MessageId = message->MessageHeader.MessageId;

        //
        //  Need to invert the boolean -- result is true if found
        //  foul language, in which case SafeToOpen should be set to false.
        //

        replyMessage.Reply.SafeToOpen = result;

		//ZeroMemory (szMsg, SIZE_STRING);
        //sprintf_s (szMsg, SIZE_STRING, "Replying message, SafeToOpen: %d", replyMessage.Reply.SafeToOpen );		
		//OutputText (szMsg);

        hr = FilterReplyMessage( Context->Port,
                                 (PFILTER_REPLY_HEADER) &replyMessage,
                                 sizeof( replyMessage ) );

        if (SUCCEEDED( hr )) {

			
            //OutputText ( "Replied message" );

        } else {
			
			ZeroMemory (szMsg, SIZE_STRING);
            sprintf_s (szMsg, SIZE_STRING, "CedeSafe Worker: Error replying message. Error = 0x%X", hr );
			OutputText (szMsg);
            break;
        }

        memset( &message->Ovlp, 0, sizeof( OVERLAPPED ) );

        hr = FilterGetMessage( Context->Port,
                               &message->MessageHeader,
                               FIELD_OFFSET( SCANNER_MESSAGE, Ovlp ),
                               &message->Ovlp );

        if (hr != HRESULT_FROM_WIN32( ERROR_IO_PENDING )) {

            break;
        }
    }

    if (!SUCCEEDED( hr )) {

        if (hr == HRESULT_FROM_WIN32( ERROR_INVALID_HANDLE )) {

            //
            //  Scanner port disconncted.
            //

			ZeroMemory (szMsg, SIZE_STRING);
            sprintf_s (szMsg, SIZE_STRING, "CedeSafe Comms: Port is disconnected, probably due to scanner filter unloading." );
			OutputText (szMsg);

        } else {

			//ZeroMemory (szMsg, SIZE_STRING);
            //sprintf_s (szMsg, SIZE_STRING, "Scanner: Unknown error occured. Error = 0x%X", hr );
			//OutputText (szMsg);

        }
    }

    free( message );

    return hr;
}

DWORD WINAPI MainScannerProc (PVOID pParam)
{
    DWORD requestCount = SCANNER_DEFAULT_REQUEST_COUNT;
    DWORD threadCount = SCANNER_DEFAULT_THREAD_COUNT;
    HANDLE threads[SCANNER_MAX_THREAD_COUNT];
    SCANNER_THREAD_CONTEXT context;
    HANDLE port, completion;
    PSCANNER_MESSAGE msg;
    DWORD threadId;
    DWORD retVal = 0;
    HRESULT hr;
    DWORD i, j;
	char szMsg[SIZE_STRING];

    //
    //  Open a commuication channel to the filter
    //

    OutputText ( "Realtime Engine: Connecting to the filter driver..." );

    hr = FilterConnectCommunicationPort( ScannerPortName,
                                         0,
                                         NULL,
                                         0,
                                         NULL,
                                         &port );

    if (IS_ERROR( hr )) {


		ZeroMemory (szMsg, SIZE_STRING);
		sprintf_s (szMsg, SIZE_STRING, "ERROR: Connecting to filter port: 0x%08x", hr);
        OutputText (szMsg);
        return 2;
    }

    //
    //  Create a completion port to associate with this handle.
    //

    completion = CreateIoCompletionPort( port,
                                         NULL,
                                         0,
                                         threadCount );

    if (completion == NULL) {

		ZeroMemory (szMsg, SIZE_STRING);
        sprintf_s (szMsg, SIZE_STRING, "ERROR: Creating completion port: %d", GetLastError() );
		OutputText (szMsg);
        CloseHandle( port );
        return 3;
    }

	ZeroMemory (szMsg, SIZE_STRING);
    sprintf_s (szMsg, SIZE_STRING,  "Realtime Engine: Port = 0x%p Completion = 0x%p", port, completion );
	OutputText (szMsg);

    context.Port = port;
    context.Completion = completion;


    //
    //  Create specified number of threads.
    //

    for (i = 0; i < threadCount; i++) {

        threads[i] = CreateThread( NULL,
                                   0,
                                    (LPTHREAD_START_ROUTINE) ScannerWorker,
                                   &context,
                                   0,
                                   &threadId );

        if (threads[i] == NULL) {

            //
            //  Couldn't create thread.
            //

            hr = GetLastError();
			
			ZeroMemory (szMsg, SIZE_STRING);
            sprintf_s (szMsg, SIZE_STRING, "ERROR: Couldn't create thread: %d", hr );
			OutputText (szMsg);

            goto main_cleanup;
        }

        for (j = 0; j < requestCount; j++) {

            //
            //  Allocate the message.
            //

            msg = (SCANNER_MESSAGE *) malloc( sizeof( SCANNER_MESSAGE ) );

            if (msg == NULL) {

                hr = ERROR_NOT_ENOUGH_MEMORY;
                goto main_cleanup;
            }

            memset( &msg->Ovlp, 0, sizeof( OVERLAPPED ) );

            //
            //  Request messages from the filter driver.
            //

            hr = FilterGetMessage( port,
                                   &msg->MessageHeader,
                                   FIELD_OFFSET( SCANNER_MESSAGE, Ovlp ),
                                   &msg->Ovlp );

            if (hr != HRESULT_FROM_WIN32( ERROR_IO_PENDING )) {

                free( msg );
                goto main_cleanup;
            }
        }
    }

    hr = S_OK;

    WaitForMultipleObjects( i, threads, TRUE, INFINITE );

main_cleanup:

	ZeroMemory (szMsg, SIZE_STRING);
    sprintf_s (szMsg, SIZE_STRING, "Realtime Engine:  All done. Result = 0x%08x", hr );
	OutputText (szMsg);

    CloseHandle( port );
    CloseHandle( completion );

	PostMessage (g_hwnd, WM_ENGINERESTART, 0, 0);

    return hr;
}

void DoMainScannerThread ()
{
	
	//Beep (1000, 200);
	HANDLE hThread;
	DWORD dwThreadID;

	hThread = CreateThread (NULL, 0, MainScannerProc, 0, 0, &dwThreadID);
	//MessageBox (NULL, "Thread", "Info", MB_OK);
}


DWORD WINAPI EncryptEventProc (PVOID pParam)
{
	EncryptEvent encEvent;

	int ieventid = 0;
	int t = 0;
	memcpy (&ieventid, pParam, sizeof (int));

	//OutputText ("THREAD: Thread proc 2 worker.");
	OutputInt ("THREAD ID: ", ieventid);
	
	encEvent = m_fixedcryptevents[ieventid];

	OutputText ("THREAD: Encrypting file: ", encEvent.szFilepath);
	
	// Now we start our internal timer of 7 seconds
	while (encEvent.iTimer < 7000) {
		encEvent = m_fixedcryptevents[ieventid];
		encEvent.iTimer += 50;
		
		if (encEvent.bCancel == true) {
			encEvent.bInuse = false;
			ZeroMemory (encEvent.szFilepath, SIZE_STRING);
			encEvent.bCancel = false;
			m_fixedcryptevents[ieventid] = encEvent;
			OutputInt ("THREAD: Thread cancelled, Thread ID: ", ieventid);
			return 0;
		}

		m_fixedcryptevents[ieventid] = encEvent;
		//OutputInt ("THREAD: Encryption delay countup: ", encEvent.iTimer);
		Sleep (50);
	}
	
	// If we get here then the timer is up, and now we are ready to encrypt the file
	
	unsigned int encRes = g_enc.IsFileEncrypted (encEvent.szFilepath);

	if (encRes == CRYPTRES_FAILED) {
		OutputInt ("THREAD: Crypt Res failed!, threadid: ", ieventid);
	} 

	if (encRes == CRYPTRES_ENCRYPTED) {
		OutputInt ("THREAD: File IS encrypted, threadid: ", ieventid);
	}

	if (encRes == CRYPTRES_NOTENCRYPTED) {
		OutputInt ("THREAD: File is NOT encrypted, threadid: ", ieventid);
		
		encEvent.cipherinprogress = true;
		m_fixedcryptevents[ieventid] = encEvent;		
		
		if (g_bsessionpasswordok == true) {

			//PostMessage (g_hwnd, WM_STARTTRAYANIM, 0, 0);

			if (g_bEngineenabled == true) {
				if (g_enc.EncryptFileEx2 (encEvent.szFilepath, encEvent.szFilepath, g_sessionpassword, true) == true) {
					OutputInt ("THREAD: File Encrypted successfully. Thread ID: ", ieventid);
					LogOutputText ("ENCRYPTED FILE: ", encEvent.szFilepath);
				} else {
					OutputInt ("THREAD: File Encryption failed. Thread ID: ", ieventid);
					LogOutputText ("ENCRYPTION FAILED: ", encEvent.szFilepath);
				}
			} else {
				SetAlertText ("Encryption has not taken place because Realtime Encryption has been disabled.");
				PostMessage (g_hwnd, WM_ACTIVATEALERT, 0, 0);			
			}
			

			//PostMessage (g_hwnd, WM_STOPTRAYANIM, 0, 0);
		} else {
						
			SetAlertText ("Encryption has not taken place because you have not specified a password.");
			PostMessage (g_hwnd, WM_ACTIVATEALERT, 0, 0);
		}
		

		encEvent.cipherinprogress = false;
		m_fixedcryptevents[ieventid] = encEvent;
	}

	// Now ensure that the encryption event is freed up so another thread can use it
	encEvent.iTimer = 0;
	encEvent.bInuse = false;
	ZeroMemory (encEvent.szFilepath, SIZE_STRING);

	// Put our modified event back into the fixed global array
	m_fixedcryptevents[ieventid] = encEvent;

	OutputInt ("THREAD: Thread exited, ThreadID: ", ieventid);

	return 0;
}

void CreateEventThread (int eventID)
{
	int id = eventID;

	HANDLE hThread;
	DWORD dwThreadID;
	hThread = CreateThread (NULL, 0, EncryptEventProc, &id, 0, &dwThreadID);
	
}

DWORD WINAPI EncryptCreatorProc (PVOID pParam)
{
	// Scanner Worker simulation thread
	// This is where the encrypt events are created.
	EncryptEvent newEncrypt;
	int ifixedpointer = 0;

	ifixedpointer = GetNewCryptEventID ();

	ZeroMemory (newEncrypt.szFilepath, SIZE_STRING);
	strcpy_s (newEncrypt.szFilepath, SIZE_STRING, "C:\\Temp\\TheTestFile.doc");

	m_fixedcryptevents[ifixedpointer] = newEncrypt;

	// Put the new encrypt event in the static array, send a message to the main thread to add it
	// to the dynamic list, and creates a thread to encrypt the file.

	return 0;
}

void SpawnEncryptEvent ()
{
	HANDLE hThread;
	DWORD dwThreadID;
	hThread = CreateThread (NULL, 0, EncryptCreatorProc, 0, 0, &dwThreadID);
}

bool TestWriteFile ()
{
	int testvalue = 123;
	int testvalue2 = 589;
	FILE *hDestination; 
	if(hDestination = fopen("C:\\Temp\\TestWrite.dat","wb")) {
		 //printf("Destination file %s is open. \n", szDestination);
	} else {
		OutputText ("Error opening output file.");
		return false;
	}

	fwrite(&testvalue, 1, sizeof (int), hDestination); 
	if(ferror(hDestination)) { 
		OutputText ("Error writing data.");
		return false;
	}

	fwrite(&testvalue2, 1, sizeof (int), hDestination); 
	if(ferror(hDestination)) { 
		OutputText ("Error writing data 2.");
		return false;
	}

	if(fclose(hDestination)) {
		OutputText ("Error closing output file.");
		return false;
	}

	OutputText ("File written ok.");
	return true;
}


void SetupSysTrayIcons ()
{
	m_hSystrayicon = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ICONNORM));
	m_hSystrayiconleft = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ICONLEFT));
	m_hSystrayiconright = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ICONRIGHT));
	m_hSystrayicontop = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ICONTOP));
	m_hSystrayiconbottom = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ICONBOTTOM));
	m_hSystrayiconerror = LoadIcon (GetModuleHandle (NULL), MAKEINTRESOURCE (ICONERROR));
}

void AddTrayIcon ()
{
	m_nid.cbSize = sizeof (m_nid);
	m_nid.hWnd = g_hwnd;
	m_nid.uID = IDC_SYSTRAYICON;
	m_nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	m_nid.uCallbackMessage = WM_SYSTRAY;
	m_nid.hIcon = m_hSystrayicon;

	strcpy (m_nid.szTip, "CedeCrypt Realtime Encryption");

	// Now actually create the system tray icon.
	Shell_NotifyIcon (NIM_ADD, &m_nid);
}

void UpdateTrayIcon (int iFrame)
{
	m_nid.cbSize = sizeof (m_nid);
	m_nid.hWnd = g_hwnd;
	m_nid.uID = IDC_SYSTRAYICON;
	m_nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	m_nid.uCallbackMessage = WM_SYSTRAY;
	if (iFrame == 0) {
		m_nid.hIcon = m_hSystrayiconleft;
	}
	
	if (iFrame == 1) {
		m_nid.hIcon = m_hSystrayicontop;
	}

	if (iFrame == 2) {
		m_nid.hIcon = m_hSystrayiconright;
	}

	if (iFrame == 3) {
		m_nid.hIcon = m_hSystrayiconbottom;
	}

	if (iFrame == 5) {
		m_nid.hIcon = m_hSystrayicon;
	}

	if (iFrame == 6) {
		m_nid.hIcon = m_hSystrayiconerror;
	}

	strcpy (m_nid.szTip, "CedeCrypt Realtime Encryption");

	// Now actually create the system tray icon.
	Shell_NotifyIcon (NIM_MODIFY, &m_nid);
}

void DeleteTrayIcon ()
{
	m_nid.cbSize = sizeof (m_nid);
	m_nid.hWnd = g_hwnd;
	m_nid.uID = IDC_SYSTRAYICON;
	m_nid.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	m_nid.uCallbackMessage = WM_SYSTRAY;
	m_nid.hIcon = m_hSystrayicon;

	strcpy (m_nid.szTip, "CedeCrypt Realtime Encryption");

	// Now actually create the system tray icon.
	Shell_NotifyIcon (NIM_DELETE, &m_nid);
}


void ShowInfoBalloon (char *szText)
{
	//NOTIFYICONDATA nid;
	
	m_nid.cbSize = sizeof (m_nid);	
	m_nid.uID = IDC_SYSTRAYICON;
	m_nid.hWnd = g_hwnd;

	m_nid.uFlags = NIF_INFO;
	
	m_nid.uTimeout = 5000;
	m_nid.dwInfoFlags = NIIF_INFO;
		
	strcpy (m_nid.szInfo, szText);
	strcpy (m_nid.szInfoTitle, "CedeCrypt");

	// Now actually create the system tray icon.
	Shell_NotifyIcon (NIM_MODIFY, &m_nid);
}

void AnimateTrayIconFullProc ()
{

	UpdateTrayIcon (m_ianimframe);
	m_ianimframe++;

	if (m_ianimframe > 3) {
		m_ianimframe = 0;
	}
}

void StartFullTrayAnim ()
{
	if (m_bstopanimtimeractive == true) {
		m_bstopanimtimeractive = false;
		KillTimer (g_hwnd, IDT_TIMERTRAYANIMSTOP);
	} else {
		if (m_banimtimeractive == false) {
			SetTimer (g_hwnd, IDT_TIMERTRAYANIMFULL, 100, NULL);
			m_banimtimeractive = true;
		}
	}
	
}

void StopFullTrayAnim ()
{
	//KillTimer (g_hwnd, IDT_TIMERTRAYANIMFULL);
	//UpdateTrayIcon (5);

	if (m_bstopanimtimeractive == false) {
		m_bstopanimtimeractive = true;
		SetTimer (g_hwnd, IDT_TIMERTRAYANIMSTOP, 2000, NULL);
	}
}


void DoPasswordPrompt ()
{
	if (m_reg.DoesSettingExist ("RteVerify") == true) {
		// RteVerify is a nice name for the MD5 password hash of the users password
		// It's pure use is to determine whether to display the password dialog in "new" mode or not
		// and to give an indication to the user whether they entered the correct realtime password
		
		m_passwindow.Initialise (g_hwnd, 0);
		m_passwindow.SetEncryptMode (false);
		m_passwindow.Show ();

	} else {
		// The hash was not found in the registry
		m_passwindow.Initialise (g_hwnd, 0);
		m_passwindow.SetEncryptMode (true);
		m_passwindow.Show ();
	}
}

void VerifyPassword ()
{
	//OutputText ("Password OK message received.");
	//OutputText (m_passwindow.GetLastPassword ());
	if (m_reg.DoesSettingExist ("RteVerify") == true) {
		// If we have the hash we need to verify what the user has entereed
		// with the hash.
		char szhash[SIZE_STRING];
		ZeroMemory (szhash, SIZE_STRING);
		strcpy_s (szhash, SIZE_STRING, m_reg.ReadStringSetting ("RteVerify"));

		char szuserinput[SIZE_STRING];
		ZeroMemory (szuserinput, SIZE_STRING);
		strcpy_s (szuserinput, SIZE_STRING, m_passwindow.GetLastPassword ());

		char szuserhash[SIZE_STRING];
		ZeroMemory (szuserhash, SIZE_STRING);
		
		if (g_enc.GetMD5Hash (szuserinput, szuserhash) == true) {
			// Now compare the hash values to see if the user input is correct
			if (strcmp (szhash, szuserhash) == 0) {								
				ZeroMemory (g_sessionpassword, SIZE_STRING);
				strcpy_s (g_sessionpassword, SIZE_STRING, szuserinput);
				g_bsessionpasswordok = true;
			} else {
				MessageBox (g_hwnd, "Incorrect password. Without the correct password you will not have access to Encrypted data.", "Access Denied.", MB_ICONEXCLAMATION | MB_OK);
				DoPasswordPrompt ();

			}
		} else {
			MessageBox (g_hwnd, "Input Hashing failed! Please contact CedeSoft", "Hashing Error", MB_ICONEXCLAMATION | MB_OK);
		}

	} else {
		// If we don't have the hash in the registry then we store what the user
		// has entered in the registry as the new hash.
		char szuserinput[SIZE_STRING];
		ZeroMemory (szuserinput, SIZE_STRING);
		strcpy_s (szuserinput, SIZE_STRING, m_passwindow.GetLastPassword ());

		char szuserhash[SIZE_STRING];
		ZeroMemory (szuserhash, SIZE_STRING);

		if (g_enc.GetMD5Hash (szuserinput, szuserhash) == true) {
			
			// Now store this hash in the registry, and also store it for use as our
			// session password

			m_reg.WriteStringSetting ("RteVerify", szuserhash);
			
			ZeroMemory (g_sessionpassword, SIZE_STRING);
			strcpy_s (g_sessionpassword, SIZE_STRING, szuserinput);
			g_bsessionpasswordok = true;

		} else {
			MessageBox (g_hwnd, "Input Hashing failed! Please contact CedeSoft", "Hashing Error", MB_ICONEXCLAMATION | MB_OK);
		}
	}
}




void GetScreenResolution ()
{
	HDC hScrDC;
	hScrDC = CreateDC("DISPLAY", NULL, NULL, NULL);
	if (hScrDC == NULL) {
		MessageBox (NULL, "Could not Retrieve Screen Resolution.", "CopyScreen", MB_OK);
	} else {
		g_iScreenResX = GetDeviceCaps(hScrDC, HORZRES);
		g_iScreenResY = GetDeviceCaps(hScrDC, VERTRES);
	}
	DeleteDC (hScrDC);
}


#pragma region Encryption Log Dialog
BOOL CALLBACK EnclogDlgProc (HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch (Message)
	{
		case WM_INITDIALOG:
		{
			HFONT hfDefault;
			hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);

			m_hwndencloglist = CreateWindow ("listbox", NULL, WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL | WS_BORDER, 10, 10, 730, 293, hWnd, (HMENU) ID_DIAGLIST, GetModuleHandle (NULL), NULL) ;
			SendMessage (m_hwndencloglist, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
			//OutputInt ("Diagnostics Ready: ", 0);

			LogOutputText ("Encryption Log: ", "Ready.");
		}
		return TRUE;
		case WM_LBUTTONUP:
		{

		}
		break;
		case WM_COMMAND:
		{
			switch (LOWORD (wParam))
			{
				case IDOK:
				{
					EndDialog (hWnd, IDOK);
				}
				break;
				case IDCANCEL:
				{
					EndDialog (hWnd, IDCANCEL);
				}
				break;
			}
		}
		break;
		default:
			return FALSE;
	}
	return TRUE;
}

void CreateEncLogDialog ()
{
	g_hwndEnclogDialog = CreateDialog (GetModuleHandle (NULL), MAKEINTRESOURCE (IDD_ENCLOGDIALOG), g_hwnd, EnclogDlgProc);
	//SetWindowPos (g_hwndAlertPopup, HWND_TOPMOST, g_iScreenResX, g_iScreenResY-65, 0, 0, SWP_NOSIZE);
	//SetWindowPos (g_hwndAlertPopup, HWND_TOPMOST, 100, 100, 0, 0, SWP_NOSIZE);	
	
}
#pragma endregion


#pragma region About Window
BOOL CALLBACK AboutDlgProc (HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch (Message)
	{
		case WM_INITDIALOG:
		{
			g_hbmAboutBanner = LoadBitmap (GetModuleHandle (NULL), MAKEINTRESOURCE (IDB_PASSBANNER));
			if (g_hbmAboutBanner == NULL) {
				MessageBox (NULL, "Could not load bitmap resource!", "Error", MB_OK);
			}
		}
		return TRUE;
		case WM_PAINT:
		{
			// Paint the box to the screen
			PAINTSTRUCT ps;
			BITMAP bm;
			HDC hdc = BeginPaint (hWnd, &ps);

			HDC hdcMem = CreateCompatibleDC (hdc);
			HBITMAP hbmOld = (HBITMAP) SelectObject (hdcMem, g_hbmAboutBanner);

			GetObject (g_hbmAboutBanner, sizeof (bm), &bm);
			
			BitBlt (hdc, 0, 0, 130, 153, hdcMem, 0, 0, SRCCOPY);

			SelectObject (hdcMem, hbmOld);
			DeleteDC (hdcMem);
			DeleteDC (hdc);
			DeleteObject (hbmOld);
			EndPaint (hWnd, &ps);
		}
		break;
		case WM_LBUTTONUP:
		{

		}
		break;
		case WM_COMMAND:
		{
			switch (LOWORD (wParam))
			{
				case IDOK:
				{
					EndDialog (hWnd, IDOK);
				}
				break;
				case IDCANCEL:
				{
					EndDialog (hWnd, IDCANCEL);
				}
				break;
			}
		}
		break;
		default:
			return FALSE;
	}
	return TRUE;
}

void CreateAboutDialog ()
{
	g_hwndAboutDialog = CreateDialog (GetModuleHandle (NULL), MAKEINTRESOURCE (IDD_ABOUTDIALOG), g_hwnd, AboutDlgProc);
	//SetWindowPos (g_hwndAlertPopup, HWND_TOPMOST, g_iScreenResX, g_iScreenResY-65, 0, 0, SWP_NOSIZE);
	//SetWindowPos (g_hwndAlertPopup, HWND_TOPMOST, 100, 100, 0, 0, SWP_NOSIZE);	
	ShowWindow (g_hwndAboutDialog, SW_SHOW);
}
#pragma endregion

#pragma region Popup Alert Dialog

HBITMAP CaptureWindowToBitmap (HWND hWnd, int xPos, int yPos, int width, int height)
{
	HDC hdc = GetDC (hWnd);
	int bRes = 1;

	if (hdc == NULL) {
		//MessageBox (NULL, "Could not get DC", "Error", MB_OK);
		Beep (1000, 5);
		return NULL;
	}
	
	HBITMAP hbmScreen = CreateCompatibleBitmap (hdc, width, height);
	if (hbmScreen == NULL) {
		//MessageBox (NULL, "Could not Create hbmBitmap", "Error", MB_OK);
		Beep (1000, 5);
		return NULL;
	}

	HDC memDC = CreateCompatibleDC (hdc);
	if (memDC == NULL) {
		Beep (1000, 5);
		//MessageBox (NULL, "Could not Create memDC", "Error", MB_OK);
		return NULL;
	}

	HBITMAP hbmOld = (HBITMAP) SelectObject(memDC, hbmScreen);
	if (!hbmOld) {
		//MessageBox (NULL, "Could not select hbmOld", "Error", MB_OK);
		Beep (1000, 5);
		return NULL;
	}

	bRes = BitBlt(memDC, 0, 0, width, height, hdc, xPos, yPos, SRCCOPY);
	if (bRes == 0) {
		//MessageBox (NULL, "Could not Blit Scr DC", "Error", MB_OK);
		Beep (1000, 5);
		return NULL;
	}

	hbmScreen = (HBITMAP) SelectObject(memDC, hbmOld);
	if (!hbmScreen) {
		//MessageBox (NULL, "Could not select hBitmap 2", "Error", MB_OK);
		//Beep (1000, 5);
		return NULL;
	}
	
	DeleteDC (memDC);
	DeleteDC (hdc);
	DeleteObject (hbmOld);

	return hbmScreen;
}

BOOL CALLBACK PopupAlertDlgProc (HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch (Message)
	{
		case WM_INITDIALOG:
		{
			SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 300, 85, SWP_NOMOVE);

			g_hbmAlertbitmap = LoadBitmap (GetModuleHandle (NULL), MAKEINTRESOURCE (IDB_ALERTBANNER));
			if (g_hbmAlertbitmap == NULL) {
				MessageBox (NULL, "Could not load bitmap resource!", "Error", MB_OK);
			}
			
			HFONT hfDefault;
			hfDefault = (HFONT) GetStockObject(DEFAULT_GUI_FONT);
			
			g_hwndlblPopup = CreateWindow ("static", "", WS_CHILD | WS_VISIBLE, 60, 23, 220, 55, hWnd, (HMENU) ID_LBLALERT, GetModuleHandle (NULL), NULL);
			SendMessage(g_hwndlblPopup, WM_SETFONT, (WPARAM)hfDefault, (LPARAM) 0);


		}
		return TRUE;
		case WM_PAINT:
		{
			// Paint the box to the screen
			PAINTSTRUCT ps;
			BITMAP bm;
			HDC hdc = BeginPaint (hWnd, &ps);

			HDC hdcMem = CreateCompatibleDC (hdc);
			HBITMAP hbmOld = (HBITMAP) SelectObject (hdcMem, g_hbmAlertbitmap);

			GetObject (g_hbmAlertbitmap, sizeof (bm), &bm);
			
			BitBlt (hdc, 0, 0, 300, 85, hdcMem, 0, 0, SRCCOPY);

			SelectObject (hdcMem, hbmOld);
			DeleteDC (hdcMem);
			DeleteDC (hdc);
			DeleteObject (hbmOld);
			EndPaint (hWnd, &ps);
		}
		break;
		case WM_CTLCOLORSTATIC:
		{
			/*HBRUSH hBrush;

			hBrush = CreatePatternBrush (g_hbmAlertbitmap);
			SetTextColor ((HDC) wParam, RGB (220, 220, 220));
			SetBkMode ((HDC) wParam, TRANSPARENT);

			return (LRESULT) hBrush;*/

			RECT ctlrect;
			GetClientRect ((HWND) lParam, &ctlrect);

			HBRUSH hBrush;

			hBrush = CreatePatternBrush (CaptureWindowToBitmap ((HWND) lParam, ctlrect.left, ctlrect.top, ctlrect.right-ctlrect.left, ctlrect.bottom-ctlrect.top));
			SetTextColor ((HDC) wParam, RGB (220, 220, 220));
			SetBkMode ((HDC) wParam, TRANSPARENT);

			return (LRESULT) hBrush;
		}
		break;
		case WM_LBUTTONUP:
		{
			AlertAnimHide ();
		}
		break;
		case WM_COMMAND:
		{
			switch (LOWORD (wParam))
			{
				case IDOK:
				{
					EndDialog (hWnd, IDOK);
				}
				break;
				case IDCANCEL:
				{
					EndDialog (hWnd, IDCANCEL);
				}
				break;
			}
		}
		break;
		default:
			return FALSE;
	}
	return TRUE;
}

void CreateAlertPopupDialog ()
{
	g_hwndAlertPopup = CreateDialog (GetModuleHandle (NULL), MAKEINTRESOURCE (IDD_ALERTPOPUP), g_hwnd, PopupAlertDlgProc);	
	//SetWindowPos (g_hwndAlertPopup, HWND_TOPMOST, g_iScreenResX, g_iScreenResY-65, 0, 0, SWP_NOSIZE);
	//SetWindowPos (g_hwndAlertPopup, HWND_TOPMOST, 100, 100, 0, 0, SWP_NOSIZE);
	SetWindowPos (g_hwndAlertPopup, HWND_TOPMOST, g_iScreenResX+300, g_iScreenResY-85, 0, 0, SWP_NOSIZE);
	ShowWindow (g_hwndAlertPopup, SW_SHOW);
}

void AlertAnimShow ()
{
	if (g_bAlertdialogcreated == false) {
		CreateAlertPopupDialog ();
		g_bAlertdialogcreated = true;
	}

	//IDT_ALERTANIMSHOW
	//IDT_ALERTANIMHIDE
	if (g_bAlertdialogvisible == false) {
		ShowWindow (g_hwndAlertPopup, SW_SHOW);

		SetDlgItemText (g_hwndAlertPopup, ID_LBLALERT, szAlertmessage);

		SetTimer (g_hwnd, IDT_ALERTANIMSHOW, 15, NULL);
 		g_ianimxpos = 0;
	}
}

void SetAlertText (char *szText)
{
	ZeroMemory (szAlertmessage, SIZE_STRING);
	strcpy_s (szAlertmessage, SIZE_STRING, szText);

	//SetDlgItemText (g_hwndAlertPopup, ID_LBLALERT, szText);
}

void SetAlertText (char *szProcess, char *szText)
{
	ZeroMemory (szAlertmessage, SIZE_STRING);
	sprintf_s (szAlertmessage, "%s %s", szProcess, szText);
	//SetDlgItemText (g_hwndAlertPopup, ID_LBLALERT, szFinalMessage);
}

void AlertAnimHide ()
{
	if (g_bAlertdialogvisible == true) {
		SetTimer (g_hwnd, IDT_ALERTANIMHIDE, 15, NULL);
	}
}

void AlertHideProc ()
{
	g_ianimxpos-=5;

	SetWindowPos (g_hwndAlertPopup, HWND_TOPMOST, g_iScreenResX-g_ianimxpos, g_iScreenResY-85, 0, 0, SWP_NOSIZE);

	if (g_ianimxpos < 0) {
		KillTimer (g_hwnd, IDT_ALERTANIMHIDE);
		ShowWindow (g_hwndAlertPopup, SW_HIDE);
		g_bAlertdialogvisible = false;
	}
}

void AlertShowProc ()
{
	g_ianimxpos+=5;

	SetWindowPos (g_hwndAlertPopup, HWND_TOPMOST, g_iScreenResX-g_ianimxpos, g_iScreenResY-85, 0, 0, SWP_NOSIZE);

	if (g_ianimxpos > 295) {
		KillTimer (g_hwnd, IDT_ALERTANIMSHOW);
		g_bAlertdialogvisible = true;
	}
}

#pragma endregion

LRESULT WINAPI WndProc (HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if (msg == g_ipcmessage) {
		// If we've received a global message to shut down
		// in the event of an uninstall.
		DeleteTrayIcon ();
		PostQuitMessage (0);
	}
    switch( msg )
    {
		case WM_OPTIONSCLOSED:
		{
			OutputText ("Options Dialog closed.");
			//m_dlincludeddirectories
			m_options.GetConfig (&m_dlwantedextensions, &m_dlfilteredapps, &m_dlfiltereddirectories, &m_dlincludeddirectories, &m_settings);
			
			m_settings.Save ();

			m_cmf.SIDLToFile (&m_dlwantedextensions, "Filetypes.dat");
			m_cmf.SIDLToFile (&m_dlfilteredapps, "Applications.dat");
			m_cmf.SIDLToFile (&m_dlfiltereddirectories, "Folderexclusions.dat");
			m_cmf.SIDLToFile (&m_dlincludeddirectories, "Folderinclusions.dat");

		}
		break;
		case WM_STARTTRAYANIM:
		{
			StartFullTrayAnim ();
		}
		break;
		case WM_STOPTRAYANIM:
		{
			StopFullTrayAnim ();
		}
		break;
		case WM_ACTIVATEALERT:
		{
			AlertAnimShow ();
		}
		break;
		case WM_SYSTRAY:
		{
			UINT uMouseMsg;

			uMouseMsg = (UINT) lParam;
			if (uMouseMsg == WM_RBUTTONDOWN) {
				POINT mousepoints;
				GetCursorPos (&mousepoints);
				SetForegroundWindow (m_nid.hWnd); // Without this, you can't dismiss the popup menu without clicking an option (apparently a bug in windows)
				TrackPopupMenuEx (m_hTrayMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON, mousepoints.x, mousepoints.y, hWnd, NULL);
			}

			//if (uMouseMsg == WM_LBUTTONDBLCLK) {
				//Show ();
			//}
		}
		break;
		case WM_ACTIVATE:
		{
			switch (wParam)
			{
				case WA_INACTIVE:
				{
					//MessageBox (NULL, "App Deactivated!", "Deactive", MB_OK);
				}
				break;
			}
		}
		break;
		case WM_ENGINERESTART:
		{
			OutputText ("The Realtime Engine was restarted.");
			DoMainScannerThread ();
		}
		break;
		case WM_ADDENCRYPTEVENT:
		{
			OutputText ("AddEncryptEvent received.");
		}
		break;
		case WM_GENEVENTID:
		{
			OutputText ("GenEvent window message received.");

			// This ensures that encryption threads waiting for a new event id
			// don't wait the same amount of time, just to avoid threads simulataneously trying to generate
			// a new event id - maximum wait is MAXFIXEDEVENTS milliseconds, so 250

			if (m_iwaitamount < MAXFIXEDEVENTS) {
				m_iwaitamount++;
			} else {
				m_iwaitamount = 1;
			}
			

			m_beventgenbusy = true;
			m_iavaileventid = GetNewCryptEventID ();
			m_beventgenbusy = false;
		}
		break;
		case WM_CREATE:
		{
			g_hwnd = hWnd; // Make the window handle global

			HFONT hfDefault;
			hfDefault = (HFONT) GetStockObject (DEFAULT_GUI_FONT);

			m_hwnddiaglist = CreateWindow ("listbox", NULL, WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL | WS_BORDER, 10, 10, 664, 293, hWnd, (HMENU) ID_DIAGLIST, GetModuleHandle (NULL), NULL) ;
			SendMessage (m_hwnddiaglist, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
			OutputInt ("Diagnostics Ready: ", 0);

			m_btntestbutton = CreateWindow ("button", "Test", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 10, 310, 70, 19, hWnd, (HMENU) ID_TESTBUTTON, GetModuleHandle (NULL), NULL);
			SendMessage (m_btntestbutton, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
			//ShowWindow (m_btntestbutton, SW_HIDE);

			m_btntestbutton2 = CreateWindow ("button", "Test 2", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 90, 310, 70, 19, hWnd, (HMENU) ID_TESTBUTTON2, GetModuleHandle (NULL), NULL);
			SendMessage (m_btntestbutton2, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));
			ShowWindow (m_btntestbutton2, SW_HIDE);

			m_btnlistevents = CreateWindow ("button", "List Crypt Events", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 190, 310, 120, 19, hWnd, (HMENU) ID_BTNLISTEVENTS, GetModuleHandle (NULL), NULL);
			SendMessage (m_btnlistevents, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

			m_btnshowalert = CreateWindow ("button", "Show Sample Alert", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, 350, 310, 120, 19, hWnd, (HMENU) ID_BTNSHOWSAMPLEALERT, GetModuleHandle (NULL), NULL);
			SendMessage (m_btnshowalert, WM_SETFONT, (WPARAM) hfDefault, MAKELPARAM (FALSE, 0));

			g_enc.SetOutputHWND (m_hwnddiaglist);

			g_ipcmessage = RegisterWindowMessage ("CedeSafe Quick IPC");

			CreateEncLogDialog ();

			GetScreenResolution ();
			SetupFilteredDirectories ();
			SetupWantedExtensions ();
			SetupFilteredApps ();

			m_settings.Load ();
			m_options.SetDefaultDirs (&m_dldefaultfiltereddirs);
			
			// Now load the lists, file types, applications, excluded folders and included folders
			if (m_cmf.LocalFileExists ("Filetypes.dat") == true) {
				m_cmf.FileToSIDL (&m_dlwantedextensions, "Filetypes.dat");
			}

			if (m_cmf.LocalFileExists ("Applications.dat") == true) {
				m_cmf.FileToSIDL (&m_dlfilteredapps, "Applications.dat");
			}

			if (m_cmf.LocalFileExists ("Folderexclusions.dat") == true) {
				m_cmf.FileToSIDL (&m_dlfiltereddirectories, "Folderexclusions.dat");
			}

			if (m_cmf.LocalFileExists ("Folderinclusions.dat") == true) {
				m_cmf.FileToSIDL (&m_dlincludeddirectories, "Folderinclusions.dat");
			}

			// Add the system tray icon
			SetupSysTrayIcons ();			
			AddTrayIcon ();			
			

			m_hTrayMenu = CreatePopupMenu ();

			AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_ENTERPASSWORD, "Enter Session Password...");
			AppendMenu (m_hTrayMenu, MF_MENUBREAK, NULL, NULL);
			AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_ENCLOG, "Encryption Log");
			AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_SHOWLOG, "Application Log");
			AppendMenu (m_hTrayMenu, MF_MENUBREAK, NULL, NULL);
			AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_ENABLE, "Enable Realtime Encryption");
			AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_DISABLE, "Disable Realtime Encryption");
			AppendMenu (m_hTrayMenu, MF_MENUBREAK, NULL, NULL);
			AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_OPTIONS, "Options...");
			AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_ABOUT, "About...");
			AppendMenu (m_hTrayMenu, MF_STRING, IDM_TRAY_SHUTDOWN, "Shutdown");
			
			/*m_al2.Initialise (hWnd, 0);
			m_al2.SetAlertText ("This is a test");
			m_al2.AnimShow ();*/

			DoPasswordPrompt ();

			DoMainScannerThread ();
		}
		break;
		case WM_UICOMMAND:
		{
			switch (wParam)
			{
				case CID_PASSWORDOK:
				{
					VerifyPassword ();
				}
				break;
				case CID_PASSWORDCANCEL:
				{
					ZeroMemory (g_sessionpassword, SIZE_STRING);
					g_bsessionpasswordok = false;

					OutputText ("Password Request Cancelled.");
					MessageBox (hWnd, "WARNING: If you do not provide a password your data will not be encrypted or decrypted. You will not have access to encrypted files, and any new/changed files will not be encrypted.", "Password Warning", MB_OK | MB_ICONEXCLAMATION);
				}
				break;
			}
		}
		break;
		case WM_COMMAND:
		{
			switch (LOWORD (wParam))
			{
				case IDM_TRAY_DISABLE:
				{
					UpdateTrayIcon (6);
					g_bEngineenabled = false;
				}
				break;
				case IDM_TRAY_ENABLE:
				{
					UpdateTrayIcon (5);
					g_bEngineenabled = true;
				}
				break;
				case IDM_TRAY_ENCLOG:
				{
					ShowWindow (g_hwndEnclogDialog, SW_SHOW);	
				}
				break;
				case IDM_TRAY_ABOUT:
				{
					CreateAboutDialog ();
				}
				break;
				case IDM_TRAY_SHOWLOG:
				{
					ShowWindow (g_hwnd, SW_SHOW);
				}
				break;
				case IDM_TRAY_ENTERPASSWORD:
				{
					DoPasswordPrompt ();
				}
				break;
				case IDM_TRAY_SHUTDOWN:
				{
					if (MessageBox (hWnd, "If you shut down the Realtime Encryption Engine, you will not have access to your Encrypted files, and your data will not be automatically encrypted. Are you sure you wish to proceed?", "Shutdown Warning", MB_ICONQUESTION | MB_YESNO) == IDYES) {
						DeleteTrayIcon ();
						PostQuitMessage (0);
					}
				}
				break;
				case IDM_TRAY_OPTIONS:
				{
					m_options.Initialise (hWnd, 0);
					m_options.SetConfig (&m_dlwantedextensions, &m_dlfilteredapps, &m_dlfiltereddirectories, &m_dlincludeddirectories, &m_settings);
					m_options.Show ();
				}
				break;
				case ID_BTNLISTEVENTS:
				{
					EncryptEvent curEvent;
					for (int e=0;e<MAXFIXEDEVENTS;e++) {
						curEvent = m_fixedcryptevents[e];

						// List out the details of this event
						OutputInt ("Details for Event index: ", e);
						OutputInt ("   - iEventID: ", curEvent.iEventID);
						OutputText("   - szFilepath: ", curEvent.szFilepath);
						if (curEvent.bInuse == true) {
							OutputText("   - bInuse: TRUE");
						} else {
							OutputText("   - bInuse: FALSE");
						}
						OutputInt ("   - iTimer: ", curEvent.iTimer);
					}				
				}
				break;
				case ID_BTNSHOWSAMPLEALERT:
				{
						SetAlertText ("This is a sample alert. This will be displayed if the Realtime Engine needs to display an urgent message.");
						PostMessage (g_hwnd, WM_ACTIVATEALERT, 0, 0);
				}
				break;
				case ID_TESTBUTTON:
				{
					//m_al2.Initialise (hWnd, 0);
					//m_al2.SetAlertText ("Winword", "will not be able to read this file because it is Encrypted and you have not specified a password.");
					//m_al2.AnimShow ();
					//CreateAlertPopupDialog ();
					
					
					//AlertAnimShow ();
					//SetAlertText ("Winword", "will not be able to read this file because it is Encrypted and you have not specified a password.");

					//OutputInt ("sizeof UIWindow: ", sizeof (UIWindow));

					//StartFullTrayAnim ();
					//StopFullTrayAnim ();

					//DynList dlList;
					//StringItem *pitem;

					//m_cmf.FileToSIDL (&dlList, "Extensions.dat");

					//for (int i=0;i<dlList.GetNumItems ();i++) {
					//	pitem = (StringItem *) dlList.GetItem (i);

					//	OutputText (pitem->szItem);
					//}
					//LogOutputText ("ENCRYPTION TEST: ", "A test file path.");
					LPCWSTR pwszPath = L"Hello this is a wide test";

					char szPath[SIZE_STRING];
					ZeroMemory (szPath, SIZE_STRING);

					LPBOOL lpUsedefault;

					if (WideCharToMultiByte (CP_ACP, WC_COMPOSITECHECK, pwszPath, -1, szPath, 1024, NULL, lpUsedefault) != 0) {
						MessageBox (hWnd, szPath, "szPath", MB_OK);
					}

				}
				break;

				case ID_TESTBUTTON2:
				{
					/*m_al2.Initialise (hWnd, 0);
					m_al2.SetAlertText ("Something has happened which made me go away.");
					m_al2.AnimHide ();*/

					m_options.Initialise (hWnd, 0);
					m_options.Show ();
				}
				break;
			}
		}
		break;
		case WM_TIMER:
		{
			switch (wParam)
			{
				case IDT_TIMERTRAYANIMFULL:
				{
					AnimateTrayIconFullProc ();
				}
				break;
				case IDT_TIMERTRAYANIMSTOP:
				{
					m_bstopanimtimeractive = false;
					m_banimtimeractive = false;
					KillTimer (g_hwnd, IDT_TIMERTRAYANIMSTOP);
					KillTimer (g_hwnd, IDT_TIMERTRAYANIMFULL);
					UpdateTrayIcon (5);
				}
				break;
				case IDT_ALERTANIMSHOW:
				{
					AlertShowProc ();
				}
				break;
				case IDT_ALERTANIMHIDE:
				{
					AlertHideProc ();
				}
				break;
			}
		}
		break;
		case WM_CLOSE:
		{
			ShowWindow (g_hwnd, SW_HIDE);
			return 0;
		}
		break;
        case WM_DESTROY:
			DeleteTrayIcon ();
            PostQuitMessage(0);
            return 0;
    }

    return DefWindowProc(hWnd, msg, wParam, lParam);
}



int WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	
	WNDCLASSEX wc;
	HWND hWnd;
	MSG msg;

	wc.cbSize = sizeof (WNDCLASSEX);
	wc.style = CS_CLASSDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0L;
	wc.cbWndExtra = 0L;
	wc.hInstance = GetModuleHandle (NULL);
	wc.hIcon = NULL;
	wc.hCursor = NULL;
	wc.hbrBackground = (HBRUSH) (COLOR_BTNFACE+1);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "ScanUserWndClass";
	wc.hIconSm = NULL;

	if (RegisterClassEx (&wc) == 0) {
		MessageBox (NULL, "Class registration failed!", "ASDWndClass", MB_OK);
		return -1;
	}
	
	hWnd = CreateWindowEx (WS_EX_WINDOWEDGE , "ScanUserWndClass", "CedeCrypt Realtime Engine v1.77", WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT, 700, 400,  NULL, NULL, wc.hInstance, NULL);

	if (hWnd == NULL) {
		MessageBox (NULL, "Window creation failed!", "Scan User", MB_OK);
		return -2;
	}

    // Show the window
    //ShowWindow(hWnd, SW_SHOWDEFAULT);
    UpdateWindow(hWnd);

    // Enter the message loop
    while( GetMessage(&msg, NULL, 0, 0 ))
    {
        TranslateMessage (&msg);
        DispatchMessage (&msg);
    }

	UnregisterClass("ScanUserWndClass", wc.hInstance);
	return 0;
}


